import * as types from "../Constants.ActionType";
var initState = {
  srcData: [],
  err_code: "",
};

var HomeReducer = (state = initState, action) => {
  switch (action.type) {
    case types.ReloadHome_Success: {
      const { data } = action;
      return { ...state, err_code: "", srcData: data };
    }
    case types.ReloadHome_Failed: {
      const { err } = action;
      return { ...state, err_code: err };
    }
    case types.Like_Success: {
      const { data } = action;
      return { ...state, err_code: "", srcData: data };
    }
    case types.Like_Failed: {
      const { err } = action;
      return { ...state, err_code: err };
    }
    case types.Comment_Status_Success: {
      const { data } = action;
      return { ...state, err_code: "", srcData: data };
    }
    case types.Comment_Status_Failed: {
      const { err } = action;
      return { ...state, err_code: err };
    }
    default:
      return state;
  }
};
export default HomeReducer;
