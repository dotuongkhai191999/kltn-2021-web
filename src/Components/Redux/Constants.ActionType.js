export const ReloadHome_Failed = "ReloadHome_Failed";
export const ReloadHome_Success = "ReloadHome_Success";

export const Like_Success = "Like_Success";
export const Like_Failed = "Like_Failed";

export const Comment_Status_Success = "Comment_Status_Success";
export const Comment_Status_Failed = "Comment_Status_Failed";

export const Get_SingleStatus = "Get_SingleStatus";
export const Get_SingleStatus_Fail = "Get_SingleStatus_Fail";
