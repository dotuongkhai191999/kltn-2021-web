import * as types from "../Constants.ActionType";
import API from "../../API/API";
export const ReloadProfile = () => {
  return async (dispatch) => {
    try {
      const params = {
        token: localStorage.getItem("UserToken"),
      };
      const route = "status/load-personal-status";
      const headers = {};
      var api = new API();
      api
        .onCallAPI("get", route, {}, params, headers)
        .then((res) => {
          if (res.data.error_code !== 0) {
            window.alert(res.data.message);
          } else {
            res.data.data.map((user) => {
              user.userCmt = [
                {
                  userId: 1,
                  username: "cơ khánh",
                  cmt_content:
                    "Xin chào, tôi tên là Cơ Khánh và tôi đến từ TPHCM",
                },
                {
                  userId: 2,
                  username: "Ngô Lan Hương",
                  cmt_content: "Xin chào, tôi tên Hương và tôi đến từ Hà Nội",
                },
                {
                  userId: 3,
                  username: "Tường Khải",
                  cmt_content: "xin chào, tôi tên Khải và tôi đến từ Đà Nẵng",
                },
              ];
              return 0;
            });
            dispatch({
              type: types.ReloadHome_Success,
              data: res.data.data,
              err: "",
            });
          }
        })
        .catch((err) => {
          dispatch({ type: types.ReloadHome_Failed, err: err });

          console.log(err);
        });
    } catch (err) {
      dispatch({ type: types.ReloadHome_Failed, err: err });
    }
  };
};
export const ReloadOtherProfile = (userId) => {
  return async (dispatch) => {
    try {
      const params = {
        user_id: userId,
        type_search: 1,
      };
      const route = "status/load-personal-status";
      const headers = {};
      var api = new API();
      api
        .onCallAPI("get", route, {}, params, headers)
        .then((res) => {
          if (res.data.error_code !== 0) {
            window.alert(res.data.message);
          } else {
            res.data.data.map((user) => {
              user.userCmt = [
                {
                  userId: 1,
                  username: "cơ khánh",
                  cmt_content:
                    "Xin chào, tôi tên là Cơ Khánh và tôi đến từ TPHCM",
                },
                {
                  userId: 2,
                  username: "Ngô Lan Hương",
                  cmt_content: "Xin chào, tôi tên Hương và tôi đến từ Hà Nội",
                },
                {
                  userId: 3,
                  username: "Tường Khải",
                  cmt_content: "xin chào, tôi tên Khải và tôi đến từ Đà Nẵng",
                },
              ];
              return 0;
            });
            dispatch({
              type: types.ReloadHome_Success,
              data: res.data.data,
              err: "",
            });
          }
        })
        .catch((err) => {
          dispatch({ type: types.ReloadHome_Failed, err: err });

          console.log(err);
        });
    } catch (err) {
      dispatch({ type: types.ReloadHome_Failed, err: err });
    }
  };
};
export const ReloadHome = () => {
  return async (dispatch) => {
    try {
      const val = localStorage.getItem("UserToken");
      var param = {
        token: val,
      };
      var route = "status/news-feed";
      var header = {
        Authorization: "bearer" + val,
      };
      var api = new API();
      api
        .onCallAPI("get", route, {}, param, header)
        .then((res) => {
          if (res.data.error_code !== 0) {
            window.alert(res.data.message);
          } else {
            res.data.data.map((user) => {
              user.userCmt = [
                {
                  userId: 0,
                  username: "cơ khánh",
                  cmt_content:
                    "Xin chào, tôi tên là Cơ Khánh và tôi đến từ TPHCM",
                  avatar:
                    "http://api.facebook-kltn.alphawolf.io/image/default.jpg",
                },
                {
                  userId: 1,
                  username: "Ngô Lan Hương",
                  cmt_content: "Xin chào, tôi tên Hương và tôi đến từ Hà Nội",
                  avatar:
                    "http://api.facebook-kltn.alphawolf.io/image/default.jpg",
                },
                {
                  userId: 2,
                  username: "Tường Khải",
                  cmt_content: "xin chào, tôi tên Khải và tôi đến từ Đà Nẵng",
                  avatar:
                    "http://api.facebook-kltn.alphawolf.io/image/default.jpg",
                },
              ];
              return 0;
            });
            dispatch({
              type: types.ReloadHome_Success,
              data: res.data.data,
              err: "",
            });
          }
        })
        .catch((err) => {
          dispatch({ type: types.ReloadHome_Failed, err: err });

          console.log(err);
        });
    } catch (err) {
      dispatch({ type: types.ReloadHome_Failed, err: err });
    }
  };
};
export const Like_Redux = (statusId, likeNum, checkLike, allStatus) => {
  return async (dispatch) => {
    try {
      const route = "status/update-status";
      const header = {
        Authorization: "bearer" + localStorage.getItem("UserToken"),
      };
      var api = new API();
      if (checkLike === false) {
        console.log(1);
        //like (checklike = fasle la chua like)
        // console.log(likeNum + 1)
        var param = {
          status_id: statusId,
          like: likeNum + 1,
        };
        api
          .onCallAPI("post", route, {}, param, header)
          .then((res) => {
            if (res.data.error_code !== 0) {
              dispatch({ type: types.Like_Failed, err: res.data.message });
            } else {
              allStatus.map((val) => {
                if (val.id === statusId) {
                  val.who_liked_status = res.data.data;
                  val.liked = true;
                  val.like_number = likeNum + 1;
                }
                return 0;
              });
              // console.log(allStatus);

              dispatch({ type: types.Like_Success, data: allStatus });
            }
          })
          .catch((err) => {
            dispatch({ type: types.Like_Failed, err: err });
          });
      } else {
        //console.log(likeNum1 - 1)
        console.log(2);
        var params = {
          status_id: statusId,
          like: likeNum - 1,
          is_unlike: 1,
        };
        api
          .onCallAPI("post", route, {}, params, header)
          .then((res) => {
            if (res.data.error_code !== 0) {
              dispatch({ type: types.Like_Failed, err: res.data.message });
            } else {
              // console.log(allStatus);
              allStatus.map((val) => {
                if (val.id === statusId) {
                  val.who_liked_status = res.data.data;
                  val.liked = false;
                  val.like_number = likeNum - 1;
                }
                return 0;
              });

              dispatch({ type: types.Like_Success, data: allStatus });
            }
          })
          .catch((err) => {
            console.log(err);
          });
      }
    } catch (err) {
      dispatch({ type: types.Like_Failed, err: err });
    }
  };
};

export const Comment_Redux = (statusId, comment, allStatus) => {
  // comment = {username, user avatar, userId, cmt_content}
  return async (dispatch) => {
    try {
      allStatus.map((val) => {
        if (val.id === statusId) val.userCmt.push(comment);
        return 0;
      });
      dispatch({ type: types.Comment_Status_Success, data: allStatus });
    } catch (err) {
      dispatch({ type: types.Comment_Status_Failed, err: err });
    }
  };
};
