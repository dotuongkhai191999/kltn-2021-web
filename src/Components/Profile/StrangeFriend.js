import React, { useEffect, useState } from "react";
import ContentStatusNoImg from "../ContentStatus/ContentStatusNoImg.js";
import PublicImage from "../HinhAnh/Public.png";
import "./profile.css";
import API from "../API/API.js";
import { ReloadOtherProfile } from "../Redux/Actions/Home.Action.js";
import { useDispatch, useSelector } from "react-redux";
function StrangeFriend(props) {
  const dispatch = useDispatch();
  const [filter, setFilter] = useState([]);
  const [searchText, setSearchText] = useState("");
  const allStatus = useSelector((state) => state.HomePage);
  const [state, setState] = useState({
    cover: "",
    avatar: "",
    data: null,
    addFriend: "https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/33EToHSZ94f.png",
    addText: "Thêm bạn bè",
    showTimeline: true,
    showFriendlist: false,
  });
  const [dataUser, setDataUser] = useState(null);
  const deleteFriend = () => {
    //"huy loi moi kb"
    var route = "request/unfriend";
    var param = {
      friend_id: props.userId,
    };
    var header = {
      Authorization: "bearer" + localStorage.getItem("UserToken"),
    };
    var api = new API();
    api
      .onCallAPI("post", route, {}, param, header)
      .then((res) => {
        if (res.data.error_code !== 0) {
          window.alert(res.data.message);
        } else {
          setState({
            ...state,
            addText: "Thêm bạn bè",
            addFriend:
              "https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/33EToHSZ94f.png",
          });
          //  console.log(res.data.data)
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
  useEffect(() => {
    const checkRelations = (userId) => {
      if (userId) {
        var route = "user/check-relationship";
        var param = {
          friend_id: userId,
        };
        var header = {
          Authorization: "bearer" + localStorage.getItem("UserToken"),
        };
        var api = new API();
        api
          .onCallAPI("get", route, {}, param, header)
          .then((res) => {
            if (res.data.error_code !== 0) {
              window.alert(res.data.message);
            } else {
              if (res.data.data === 0)
                // bạn bè
                setState({
                  ...state,
                  addText: "Bạn bè",
                  addFriend:
                    "https://static.xx.fbcdn.net/rsrc.php/v3/ye/r/c9BbXR9AzI1.png",
                });

              if (res.data.data === 1)
                // đã gửi yêu cầu kb
                setState({
                  ...state,
                  addText: "Hủy lời mời",
                  addFriend:
                    "https://static.xx.fbcdn.net/rsrc.php/v3/yr/r/aIwEkzr3XFI.png",
                });
              if (res.data.data === 2) {
                document.getElementById("replyFriend").style.display = "block";
                setState({
                  ...state,
                  addText: "Phản hồi",
                  addFriend:
                    "https://static.xx.fbcdn.net/rsrc.php/v3/ye/r/c9BbXR9AzI1.png",
                });
              } // chờ người ta xác nhận

              if (res.data.data === 3)
                // không là bạn bè
                setState({
                  ...state,
                  addText: "Thêm bạn bè",
                  addFriend:
                    "https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/33EToHSZ94f.png",
                });
            }
          })
          .catch((err) => {
            console.log(err);
          });
      }
    };
    const GetStatus = () => {
      dispatch(ReloadOtherProfile(props.userId));
    };
    const fillIntro = () => {
      const params = {
        type_search: "1",
        user_id: props.userId,
      };
      const route = "user/search-v1";
      const headers = {
        // Authorization: 'bearer' +
        //     localStorage.getItem("UserToken")
      };
      var api = new API();
      api
        .onCallAPI("get", route, {}, params, headers)
        .then((res) => {
          if (res.data.error_code !== 0) {
            window.alert(res.data.message);
          } else {
            console.log(res.data.data);
            setDataUser(res.data.data);
            // setState({
            //   ...state,
            //   dataUser: res.data.data,
            //   friend: res.data.data.friend_array
            //     ? res.data.data.friend_array.length
            //     : "",
            //   cover: res.data.data.user_cover,
            //   avatar: res.data.data.user_avatar,
            //   friendArray: res.data.data.friend_array
            //     ? res.data.data.friend_array
            //     : null,
            // });
            // console.log("res: ", res.data.data, "state", state);
          }
        })
        .catch((err) => {
          console.log(err);
        });
    };
    GetStatus();
    fillIntro();
    checkRelations(props.userId);
  }, []);  // eslint-disable-line react-hooks/exhaustive-deps

  const ShowStatus = () => {
    var data = allStatus.srcData ? allStatus.srcData : null;

    if (data) {
      return data.map((user, index) => {
        return (
          <ContentStatusNoImg
            userCmt={user.userCmt}
            notOwn="true"
            key={index}
            linkUser={user.no_sign_profile}
            indexTus={index}
            header={user.header_content}
            statusId={user.id}
            liked={user.liked}
            whoLike={user.who_liked_status}
            name={user.user_name}
            textid={"textCmt" + user.id}
            srcAVT={user.user_avatar}
            caption={user.caption}
            likes={user.like_number}
            srcCaption={user.file_uploaded}
            statusSetting={user.status_setting}
          />
        );
      });
    }
  };

  const showDataUser = () => {
    const info = dataUser;
    // console.log(state);
    if (info) {
      document.getElementById("userName").innerText = info.user_name;
      return (
        <div>
          <strong style={{ paddingLeft: "15px", fontSize: "1vw" }}>
            Email:
          </strong>
          <br />
          <p
            style={{
              paddingLeft: "15px",
              fontSize: "1vw",
              fontWeight: "700",
              color: "dodgerblue",
            }}
          >
            {info.email}
          </p>
          <strong style={{ paddingLeft: "15px", fontSize: "1vw" }}>
            Số điện thoại:
          </strong>
          <br />
          <p
            style={{
              paddingLeft: "15px",
              fontSize: "1vw",
              fontWeight: "700",
              color: "dodgerblue",
            }}
          >
            {info.phone}
          </p>
          <strong style={{ paddingLeft: "15px", fontSize: "1vw" }}>
            Ngày sinh:
          </strong>
          <br />
          <p
            style={{
              paddingLeft: "15px",
              fontSize: "1vw",
              fontWeight: "700",
              color: "dodgerblue",
            }}
          >
            {info.dOb}
          </p>
          <strong style={{ paddingLeft: "15px", fontSize: "1vw" }}>
            Giới tính:
          </strong>
          <br />
          <p
            style={{
              paddingLeft: "15px",
              fontSize: "1vw",
              fontWeight: "700",
              color: "dodgerblue",
            }}
          >
            {info.sex ? "Nam" : "Nữ"}
          </p>
        </div>
      );
    }
  };
  const filterFriend = (e) => {
    setSearchText(e.target.value);
    const friendArray = dataUser ? dataUser.friend_array : null;
    var result = [];
    const searchQuerry = e.target.value.toLowerCase();
    // console.log(searchQuerry);
    if (friendArray) {
      for (var i = 0; i < friendArray.length; i++) {
        if (friendArray[i].user_name.toLowerCase().includes(searchQuerry)) {
          result.push(friendArray[i]);
        }
      }
      console.log(result);
      setFilter(result);
    }
  };
  const showFriendlist = () => {
    const friendArray = dataUser ? dataUser.friend_array : null;
    // var classBtn = "";
    // var txtBtn = "Thêm bạn bè";
    console.log(searchText, friendArray);
    if (searchText !== "") {
      if (filter.length !== 0) {
        return filter.map((friend, index) => {
          return (
            <div key={index} className="blockFriend">
              <img
                src={friend.avatar}
                width="25px"
                height="25px"
                className="img-responsive smallAvatar"
                alt="Image1"
              />
              <div style={{ paddingTop: "26px" }}>
                <a
                  href={"/" + friend.no_sign_profile}
                  id={"friend" + index}
                  className="smallUsername"
                >
                  {friend.user_name}
                </a>
                <button className="btn btn-defaul btnFriendList">Bạn bè</button>
              </div>
            </div>
          );
        });
      }
      return (
        <div style={{ overflowWrap: "break-word", textAlignLast: "center" }}>
          <p>Không có kết quả cho: {searchText}</p>
        </div>
      );
    }
    return friendArray.map((friend, index) => {
      return (
        <div key={index} className="blockFriend">
          <img
            src={friend.avatar}
            width="25px"
            height="25px"
            className="img-responsive smallAvatar"
            alt="Image1"
          />
          <div style={{ paddingTop: "26px" }}>
            <a
              href={"/" + friend.no_sign_profile}
              id={"friend" + index}
              className="smallUsername"
            >
              {friend.user_name}
            </a>
            <button className="btn btn-defaul btnFriendList">Bạn bè</button>
          </div>
        </div>
      );
    });
  };

  const addFriend = (e) => {
    var text = e.target.innerText;
    if (text === "Thêm bạn bè") {
      document.getElementById("loaddingAdd").style.display = "block";
      var route = "request/make-friend";
      var param = {
        receiver: props.userId,
      };
      var header = {
        Authorization: "bearer" + localStorage.getItem("UserToken"),
      };
      var api = new API();
      api
        .onCallAPI("post", route, {}, param, header)
        .then((res) => {
          if (res.data.error_code !== 0) {
            window.alert(res.data.message);
          } else {
            setState({
              ...state,
              addText: "Hủy lời mời",
              addFriend:
                "https://static.xx.fbcdn.net/rsrc.php/v3/yr/r/aIwEkzr3XFI.png",
            });
            document.getElementById("loaddingAdd").style.display = "none";
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }
    if (text === "Bạn bè") {
      document.getElementById("btnModalUnf").click();
    }
    if (text === "Hủy lời mời") {
      var param1 = {
        friend_id: props.userId,
      };
      var header1 = {
        Authorization: "bearer" + localStorage.getItem("UserToken"),
      };
      var route1 = "request/delete-request-profile";
      var api1 = new API();
      api1
        .onCallAPI("post", route1, {}, param1, header1)
        .then((res) => {
          if (res.data.error_code !== 0) {
            window.alert(res.data.message);
          } else {
            setState({
              ...state,
              addText: "Thêm bạn bè",
              addFriend:
                "https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/33EToHSZ94f.png",
            });
            document.getElementById("loaddingAdd").style.display = "none";
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }
    if (text === "Phản hồi") {
      if (
        document.getElementById("dropdownRequest").className === "dropdown open"
      )
        document.getElementById("dropdownRequest").className = "dropdown";
      else
        document.getElementById("dropdownRequest").className = "dropdown open";
    }
  };
  const acceptRequest = () => {
    var route = "user/update/info";
    var params = {
      friend_id: props.userId,
      update_type: 3,
    };
    var headers = {
      Authorization: "bearer" + localStorage.getItem("UserToken"),
    };
    var api = new API();
    api
      .onCallAPI("post", route, {}, params, headers)
      .then((res) => {
        if (res.data.error_code !== 0) {
          window.alert(res.data.message);
        } else {
          document.getElementById("replyFriend").style.display = "none";
          document.getElementById("dropdownRequest").className = "dropdown";
          setState({
            ...state,
            addText: "Bạn bè",
            addFriend:
              "https://static.xx.fbcdn.net/rsrc.php/v3/ye/r/c9BbXR9AzI1.png",
          });
          var route1 = "request/delete-request-profile";
          var param1 = {
            friend_id: props.userId,
          };
          var header = {
            Authorization: "bearer" + localStorage.getItem("UserToken"),
          };
          var api1 = new API();
          api1
            .onCallAPI("post", route1, {}, param1, header)
            .then((res) => {
              if (res.data.error_code !== 0) {
                window.alert(res.data.message);
              }
            })
            .catch((err) => {
              console.log(err);
            });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const createRoom = () => {
    var route = "chat/create-chat-group";
    var params = {
      user_id: dataUser.user_id,
    };
    var headers = {
      Authorization: "bearer" + localStorage.getItem("UserToken"),
    };
    var api = new API();
    api.onCallAPI("post", route, {}, params, headers).then((res) => {
      if (res.data.error_code !== 0) {
        window.alert(res.data.message);
      } else {
        if (res.data.data) {
          window.location.href = "/chat?room=" + res.data.data;
          localStorage.setItem(
            "sender-messenger-name",
            dataUser ? dataUser.user_name : null
          );
          localStorage.setItem(
            "sender-messenger-avatar",
            dataUser ? dataUser.user_avatar : null
          );
          localStorage.setItem(
            "sender-messenger-link",
            dataUser ? dataUser.no_sign_profile : null
          );
        }
      }
    });
  };

  // return (
  //   <button
  //     style={{ marginTop: "500px" }}
  //     type="button"
  //     onClick={() => createRoom()}
  //     className="btn btn-default"
  //   >
  //     button
  //   </button>
  // );

  return (
    <div className="container mainwallProfile">
      <form onSubmit={(e) => e.preventDefault()}>
        <div>
          <div className="Facebook-timelineSection">
            <div className="CoverProfile">
              <img
                src={dataUser ? dataUser.user_cover : ""}
                className="img-responsive imgCover CoverProfile "
                alt="Image1"
              />

              <div className="infoProfile">
                <p
                  style={{ background: "mediumpurple" }}
                  id="userName"
                  className="NameAccount"
                >
                  {dataUser ? dataUser.user_name : ""}
                </p>
                <div
                  className="dropdown"
                  id="dropdownRequest"
                  style={{ zIndex: "10" }}
                >
                  <button
                    onClick={(e) => addFriend(e)}
                    className="btn btn-large btn-block btn-default btnAddFriend"
                  >
                    <span>
                      <i
                        className="fa fa-circle-o-notch fa-spin"
                        id="loaddingAdd"
                        style={{ display: "none" }}
                      ></i>
                      <img
                        src={state.addFriend}
                        className="img-responsive addfriendImg"
                        alt="addFriend"
                      />
                    </span>
                    <strong>{state.addText} </strong>
                  </button>
                  <button
                    className="btn btn-default btnAddFriend btnMessenger"
                    onClick={() => {
                      createRoom();
                    }}
                  >
                    <img
                      src="https://static.xx.fbcdn.net/rsrc.php/v3/yI/r/YIxFfN5ecJG.png"
                      className="img-responsive addfriendImg"
                      alt="Messenger"
                    />
                    Nhắn tin
                  </button>

                  <ul
                    className="dropdown-menu"
                    style={{ left: "unset", right: "12vw", top: "-3.25vh" }}
                  >
                    <li>
                      <a
                        role="button"
                        href="# "
                        onClick={() => acceptRequest()}
                      >
                        Xác nhận
                      </a>
                    </li>
                    <li>
                      <a href="#  ">Xóa lời mời</a>
                    </li>
                  </ul>
                </div>
                <button
                  id="btnModalUnf"
                  style={{ display: "none" }}
                  className="btn btn-primary"
                  data-toggle="modal"
                  data-target="#exampleModal"
                >
                  Launch demo modal
                </button>
                <div
                  className="modal fade"
                  id="exampleModal"
                  tabIndex="-1"
                  role="dialog"
                  aria-labelledby="exampleModalLabel"
                  aria-hidden="true"
                >
                  <div className="modal-dialog" role="document">
                    <div className="modal-content">
                      <div className="modal-header">
                        <h5 className="modal-title" id="exampleModalLabel">
                          Modal title
                        </h5>
                        <button
                          type="button"
                          className="close closeBtn"
                          data-dismiss="modal"
                          aria-label="Close"
                        >
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div className="modal-body">
                        Bạn có chắc chắn muốn xóa
                        {dataUser ? dataUser.user_name : ""}
                        khỏi danh sách bạn bè không?
                      </div>
                      <div
                        className="modal-footer"
                        style={{ borderTop: "unset" }}
                      >
                        <button
                          type="button"
                          className="btn btn-secondary"
                          data-dismiss="modal"
                        >
                          Hủy
                        </button>
                        <button
                          type="button"
                          data-dismiss="modal"
                          onClick={() => deleteFriend()}
                          className="btn btn-primary"
                        >
                          Xác nhận
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="avatarProFilePage">
              <img
                src={dataUser ? dataUser.user_avatar : ""}
                className="img-responsive avatarProfile "
                alt="Image1"
                style={{ borderRadius: "100%" }}
              />
            </div>

            <div className="navbar NAVinTimeline">
              <ul className="nav navbar-nav ulNavTimeline">
                <li>
                  <a
                    href="# "
                    onClick={() =>
                      setState({
                        ...state,
                        showTimeline: true,
                        showFriendlist: false,
                      })
                    }
                    className="tagAinTimeLine"
                  >
                    <strong>Dòng thời gian</strong>
                  </a>
                </li>

                <li>
                  <a
                    href="# "
                    role="button"
                    className="tagAinTimeLine"
                    onClick={() => {
                      setState({
                        ...state,
                        showTimeline: false,
                        showFriendlist: true,
                        filter: null,
                      });
                    }}
                  >
                    <strong>Bạn bè </strong>
                    {dataUser
                      ? dataUser.friend_array
                        ? dataUser.friend_array.length
                        : ""
                      : null}
                  </a>
                </li>

                <li>
                  <a href="# " className="tagAinTimeLine">
                    <strong>Ảnh</strong>
                  </a>
                </li>

                <li>
                  <a href="# " className="tagAinTimeLine">
                    <strong>Check in </strong>
                  </a>
                </li>

                <li>
                  <a href="# " className="tagAinTimeLine tagVideoTimeline">
                    <strong>Video</strong>
                  </a>
                </li>
              </ul>
            </div>

            <div style={{ margin: "10px 0", display: "none" }} id="replyFriend">
              <h3
                style={{
                  fontWeight: "700",
                  lineHeight: "1.2",
                  float: "left",
                  width: "fit-content",
                  paddingTop: "10px",
                }}
              >
                {dataUser
                  ? dataUser.user_name + " đã gửi cho bạn lời mời kết bạn"
                  : ""}
              </h3>
              <div style={{ textAlign: "right" }}>
                <button
                  type="button"
                  className="btn btn-lg"
                  style={{
                    backgroundColor: "#1877f2",
                    marginRight: "5px",
                    color: "white",
                    fontWeight: "600",
                    padding: "6px 12px",
                  }}
                >
                  Chấp nhận lời mời
                </button>
                <button
                  type="button"
                  className="btn btn-lg btn-default"
                  style={{ fontWeight: "600", padding: "6px 12px" }}
                >
                  Xóa lời mời
                </button>
              </div>
            </div>
          </div>
        </div>
      </form>
      {state.showTimeline && (
        <div className="row" style={{ marginTop: "10px" }}>
          <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <div className="row row1Intro ">
              <div className="col-xs-1 col-sm-1 col-md-1 col-lg-1 divImageIntroHeader">
                <img
                  src={PublicImage}
                  className="img-responsive imageIntro"
                  alt="Image1"
                />
              </div>
              <p
                style={{
                  fontSize: "1.5vw",
                  marginBottom: "unset",
                  fontWeight: "700",
                  color: "#3578E5",
                }}
              >
                Giới thiệu
              </p>
              <br />
              {showDataUser()}
            </div>
          </div>
          <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
            {/* {GetStatus()} */}
            {ShowStatus()}
          </div>
        </div>
      )}
      <br />
      {state.showFriendlist && (
        <div className="container-fluid" style={{ border: "1px solid black" }}>
          <div className="row" style={{ marginTop: "10px" }}>
            <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
              <h3 style={{ padding: "3px 20px" }}>Bạn bè</h3>
            </div>
            <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
              <input
                type="text"
                id="friendSearch"
                onChange={(e) => filterFriend(e)}
                placeholder="Tìm kiếm"
                className="form-control"
              />
            </div>
          </div>

          <div>
            <br />
            {showFriendlist()}
          </div>
        </div>
      )}
    </div>
  );
}

export default StrangeFriend;
