import StatusContent from "../UpStatus/StatusContent.js";
import React, { useEffect, useState } from "react";
import ContentStatusNoImg from "../ContentStatus/ContentStatusNoImg.js";
import PublicImage from "../HinhAnh/Public.png";
import API from "../API/API.js";
import "./profile.css";
import { useSelector, useDispatch } from "react-redux";
import { ReloadProfile } from "../Redux/Actions/Home.Action";
// import LazyLoad from "react-lazyload";
function UnderHeaderProfile(props) {
  const [state, setState] = useState({
    friend: "",
    showTimeline: true,
    showFriendlist: false,
    ThemAnhBia: "",
    chinhsuaProfile: "",
    Cover: "",
    avatar: "",
    coverChanging: false,
    data: null,
    dataUser: null,
    changeAvatar: false,
    changingAvatar: false,
    frmDataAvatar: new FormData(),
    frmDataCover: new FormData(),
    friendArray: [],
    filter: null,
  });
  const allStatus = useSelector((state) => state.HomePage);
  const onHandleHover = () => {
    setState({
      ...state,
      ThemAnhBia: "Thêm ảnh bìa",
    });
  };
  const onHandleHoverChange = () => {
    setState({
      ...state,
      chinhsuaProfile: "Chỉnh sửa trang cá nhân",
    });
  };
  const onHandleLeave = () => {
    setState({
      ...state,

      ThemAnhBia: "",
    });
  };
  const onHandleLeaveChange = () => {
    setState({
      ...state,
      chinhsuaProfile: "",
    });
  };
  const saveCover = () => {
    var param = {
      update_type: 2,
      type: 0,
      option: 1, //1 là cover, avartar là 0
    };
    var route = "user/update/info";
    var headers = {
      Authorization: "bearer" + localStorage.getItem("UserToken"),
    };
    var api = new API();
    api
      .onCallAPI("post", route, state.frmDataCover, param, headers)
      .then((res) => {
        if (res.data.error_code !== 0) {
          window.alert(res.data.message);
          onHadleCancelCover();
        } else {
          localStorage.setItem("Cover", res.data.data);
          setState({
            ...state,

            coverChanging: false,
          });
          window.location.reload(false);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleChangeFile = (event) => {
    var { frmDataCover } = state;
    if (event.target.files[0]) {
      frmDataCover.append("file", event.target.files[0]);
      setState({
        ...state,

        Cover: URL.createObjectURL(event.target.files[0]),
        ThemAnhBia: "",
        frmDataCover: frmDataCover,
        coverChanging: true,
      });
    }
  };
  const saveAvatar = () => {
    var param = {
      update_type: 2,
      type: 0,
      option: 0, //1 là cover, avartar là 0
    };
    var route = "user/update/info";
    var headers = {
      Authorization: "bearer" + localStorage.getItem("UserToken"),
    };

    var api = new API();
    api
      .onCallAPI("post", route, state.frmDataAvatar, param, headers)
      .then((res) => {
        if (res.data.error_code !== 0) {
          window.alert(res.data.message);
          cancelAvatar();
        } else {
          localStorage.setItem("avatar", res.data.data);
          setState({
            ...state,

            changingAvatar: false,
          });
          // console.log(res.data.data);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const handleChangeAvatar = (e) => {
    var { frmDataAvatar } = state;
    if (e.target.files[0]) {
      frmDataAvatar.append("file", e.target.files[0]);
      setState({
        ...state,

        avatar: URL.createObjectURL(e.target.files[0]),
        frmDataAvatar: frmDataAvatar,
        changingAvatar: true,
      });
    }
  };
  const cancelAvatar = () => {
    setState({
      ...state,

      avatar: "",
      changingAvatar: false,
    });
  };
  const onHadleCancelCover = () => {
    setState({
      ...state,

      Cover: "",
      coverChanging: false,
    });
    document.getElementById("FileBrowser1").value = null;
  };
  const dispatch = useDispatch();

  useEffect(() => {
    const GetStatus = () => {
      dispatch(ReloadProfile());
    };
    const fillIntro = () => {
      const params = {
        type_search: "1",
        token: localStorage.getItem("UserToken"),
      };
      const route = "user/search-v1";
      const headers = {
        Authorization: "bearer" + localStorage.getItem("UserToken"),
      };
      var api = new API();
      api
        .onCallAPI("get", route, {}, params, headers)
        .then((res) => {
          if (res.data.error_code !== 0) {
            window.alert(res.data.message);
          } else {
            // console.log(res.data.data)
            setState({
              ...state,
              dataUser: res.data.data,
              friend: res.data.data.friend_array
                ? res.data.data.friend_array.length
                : "",
              friendArray: res.data.data.friend_array,
            });
            if (localStorage.getItem("avatar") !== res.data.data.avatar) {
              localStorage.setItem("avatar", res.data.data.user_avatar);
            }
            if (localStorage.getItem("Cover") !== res.data.data.user_cover) {
              localStorage.setItem("Cover", res.data.data.user_cover);
            }
          }
        })
        .catch((err) => {
          console.log(err);
        });
    };
    GetStatus();
    fillIntro();
    // console.log(allStatus);
  }, []);  // eslint-disable-line react-hooks/exhaustive-deps

  const ShowStatus = () => {
    var users = allStatus.srcData ? allStatus.srcData : null;
    console.log(allStatus.srcData);

    if (users) {
      console.log(users);
      return users.map((user, index) => {
        return (
          <ContentStatusNoImg
            notOwn="false"
            key={index}
            indexTus={index}
            statusId={user.id}
            textid={"textCmt" + user.id}
          />
          // <ContentStatusNoImg
          //   userCmt={user.userCmt}
          //   notOwn="false"
          //   key={index}
          //   linkUser={user.no_sign_profile}
          //   indexTus={index}
          //   header={user.header_content}
          //   statusId={user.id}
          //   liked={user.liked}
          //   whoLike={user.who_liked_status}
          //   name={user.user_name}
          //   textid={"textCmt" + user.id}
          //   srcAVT={user.user_avatar}
          //   caption={user.caption}
          //   likes={user.like_number}
          //   srcCaption={user.file_uploaded}
          //   statusSetting={user.status_setting}
          // />
        );
      });
    }
  };
  const showDataUser = () => {
    var { dataUser } = state;
    if (dataUser) {
      return (
        <div>
          <strong style={{ paddingLeft: "15px", fontSize: "1vw" }}>
            Email:
          </strong>
          <br />
          <p
            style={{
              paddingLeft: "15px",
              fontSize: "1vw",
              fontWeight: "700",
              color: "dodgerblue",
            }}
          >
            {state.dataUser.email}
          </p>
          <strong style={{ paddingLeft: "15px", fontSize: "1vw" }}>
            Số điện thoại:
          </strong>
          <br />
          <p
            style={{
              paddingLeft: "15px",
              fontSize: "1vw",
              fontWeight: "700",
              color: "dodgerblue",
            }}
          >
            {state.dataUser.phone}
          </p>
          <strong style={{ paddingLeft: "15px", fontSize: "1vw" }}>
            Ngày sinh:
          </strong>
          <br />
          <p
            style={{
              paddingLeft: "15px",
              fontSize: "1vw",
              fontWeight: "700",
              color: "dodgerblue",
            }}
          >
            {state.dataUser.dOb}
          </p>
          <strong style={{ paddingLeft: "15px", fontSize: "1vw" }}>
            Giới tính:
          </strong>
          <br />
          <p
            style={{
              paddingLeft: "15px",
              fontSize: "1vw",
              fontWeight: "700",
              color: "dodgerblue",
            }}
          >
            {state.dataUser.sex ? "Nam" : "Nữ"}
          </p>
        </div>
      );
    }
  };
  const [searchText, setSearchText] = useState("");
  const filterFriend = (e) => {
    var { friendArray } = state;
    var filter = [];
    setSearchText(e.target.value);
    if (friendArray) {
      for (var i = 0; i < friendArray.length; i++) {
        if (
          friendArray[i].user_name
            .toLowerCase()
            .includes(e.target.value.toLowerCase())
        ) {
          filter.push(friendArray[i]);
        }
      }
      setState({
        ...state,

        filter: filter,
      });
    }
  };
  const showFriendlist = () => {
    var { friendArray } = state;
    var { filter } = state;
    if (searchText !== "") {
      if (filter.length !== 0) {
        return filter.map((friend, index) => {
          return (
            <div key={index} className="blockFriend">
              <img
                src={friend.avatar}
                width="25px"
                height="25px"
                className="img-responsive smallAvatar"
                alt="Image1"
              />
              <div style={{ paddingTop: "26px" }}>
                <a
                  href={"/" + friend.no_sign_profile}
                  id={"friend" + index}
                  className="smallUsername"
                >
                  {friend.user_name}
                </a>
                <button className="btn btn-defaul btnFriendList">Bạn bè</button>
              </div>
            </div>
          );
        });
      }
      return (
        <div style={{ overflowWrap: "break-word", textAlignLast: "center" }}>
          <p>Không có kết quả cho: {searchText}</p>
        </div>
      );
    }
    if (friendArray)
      return friendArray.map((friend, index) => {
        return (
          <div key={index} className="blockFriend">
            <img
              src={friend.avatar}
              width="25px"
              height="25px"
              className="img-responsive smallAvatar"
              alt="Image1"
            />
            <div style={{ paddingTop: "26px" }}>
              <a
                href={"/" + friend.no_sign_profile}
                id={"friend" + index}
                className="smallUsername"
              >
                {friend.user_name}
              </a>
              <button className="btn btn-defaul btnFriendList">Bạn bè</button>
            </div>
          </div>
        );
      });
  };

  return (
    <div className="container mainwallProfile">
      <form onSubmit={(e) => e.preventDefault()}>
        <div>
          <div className="Facebook-timelineSection">
            <div className="CoverProfile">
              <input
                type="file"
                id="FileBrowser1"
                accept="image/*"
                style={{ display: "none" }}
                onChange={handleChangeFile}
              />
              {!state.coverChanging && (
                <button
                  type="button"
                  className="btn btn-default btnAddCover"
                  onMouseEnter={onHandleHover}
                  onMouseLeave={onHandleLeave}
                  onClick={() => {
                    document.getElementById("FileBrowser1").click();
                  }}
                >
                  {state.ThemAnhBia}
                </button>
              )}

              <img
                src={state.Cover ? state.Cover : localStorage.getItem("Cover")}
                className="img-responsive imgCover CoverProfile "
                alt="Image1"
              />
              {!state.coverChanging && (
                <div className="infoProfile">
                  <p
                    style={{ background: "mediumpurple" }}
                    className="NameAccount"
                  >
                    {localStorage.getItem("UserName")}
                  </p>
                  <a
                    role="button"
                    href="/updateinfo"
                    className="btn btn-large btn-block btn-default btnSettingINFO"
                    onMouseEnter={onHandleHoverChange}
                    onMouseLeave={onHandleLeaveChange}
                  >
                    <span
                      className="glyphicon glyphicon-pencil"
                      style={{ fontSize: "15px" }}
                    ></span>
                    &nbsp;
                    <strong
                      style={{
                        textDecoration: "underline",
                      }}
                    >
                      {state.chinhsuaProfile}
                    </strong>
                  </a>
                </div>
              )}
            </div>

            <div
              className="avatarProFilePage"
              onMouseEnter={() =>
                setState({
                  ...state,
                  changeAvatar: true,
                })
              }
              onMouseLeave={() =>
                setState({
                  ...state,
                  changeAvatar: false,
                })
              }
            >
              <img
                src={
                  state.avatar ? state.avatar : localStorage.getItem("avatar")
                }
                className="img-responsive avatarProfile "
                alt="Image1"
                style={{ borderRadius: "100%" }}
              />
              <div
                style={{
                  display:
                    state.changeAvatar &&
                    !state.changingAvatar &&
                    !state.coverChanging
                      ? "block"
                      : "none",
                }}
              >
                <button
                  className="btn btn-default btnChangeAva"
                  onClick={() => {
                    document.getElementById("fileAvatar").click();
                    setState({ ...state, changeAvatar: false });
                  }}
                >
                  <i className="fas fa-camera"></i>
                  <br /> Cập nhật
                </button>
                <input
                  type="file"
                  accept="image/*"
                  style={{ display: "none" }}
                  id="fileAvatar"
                  onChange={handleChangeAvatar}
                />
              </div>
            </div>

            <div className="navbar NAVinTimeline">
              {state.changingAvatar && (
                <div>
                  <button
                    className="btn btn-info"
                    onClick={() => saveAvatar()}
                    style={{
                      margin: "10px",
                      float: "right",
                      backgroundColor: "#2d88FF",
                    }}
                  >
                    Lưu thay đổi
                  </button>
                  <button
                    onClick={() => cancelAvatar()}
                    className="btn btn-default"
                    style={{ margin: "10px 0", float: "right" }}
                  >
                    Hủy
                  </button>
                  <span
                    style={{ margin: "10px", float: "right" }}
                    className="btnSaveCover lblCongKhaiCover"
                  >
                    <strong>Công khai</strong>
                  </span>
                </div>
              )}

              <ul className="nav navbar-nav ulNavTimeline">
                {!state.coverChanging && (
                  <li>
                    <a
                      href="# "
                      onClick={() =>
                        setState({
                          ...state,
                          showTimeline: true,
                          showFriendlist: false,
                        })
                      }
                      className="tagAinTimeLine"
                    >
                      <strong>Dòng thời gian</strong>
                    </a>
                  </li>
                )}

                {!state.coverChanging && (
                  <li>
                    <a
                      href="# "
                      id="friendProfile"
                      role="button"
                      className="tagAinTimeLine"
                      onClick={() => {
                        setState({
                          ...state,

                          showTimeline: false,
                          showFriendlist: true,
                          filter: null,
                        });
                      }}
                    >
                      <strong>Bạn bè </strong>
                      {state.friend ? state.friend : ""}
                    </a>
                  </li>
                )}
                {!state.coverChanging && (
                  <li>
                    <a href="# " className="tagAinTimeLine">
                      <strong>Ảnh</strong>
                    </a>
                  </li>
                )}
                {!state.coverChanging && (
                  <li>
                    <a href="# " className="tagAinTimeLine">
                      <strong>Check in </strong>
                    </a>
                  </li>
                )}
                {!state.coverChanging && (
                  <li>
                    <a href="# " className="tagAinTimeLine tagVideoTimeline">
                      <strong>Video</strong>
                    </a>
                  </li>
                )}
              </ul>
              {state.coverChanging && (
                <div className="container-fluid divBTNSaveCover">
                  <button
                    className="btn btnSaveCover"
                    onClick={() => saveCover()}
                  >
                    <strong> Lưu thay đổi </strong>
                  </button>
                  <button
                    onClick={() => onHadleCancelCover()}
                    className="btn btnSaveCover btnHuyCover"
                  >
                    <strong> Huỷ </strong>
                  </button>
                  <span className="btnSaveCover lblCongKhaiCover">
                    <strong>Công khai</strong>
                  </span>
                </div>
              )}
            </div>
          </div>
        </div>
      </form>

      <br />

      {state.showTimeline && (
        <div className="row" id="DongThoiGian">
          <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <div className="row row1Intro ">
              <div className="col-xs-1 col-sm-1 col-md-1 col-lg-1 divImageIntroHeader">
                <img
                  src={PublicImage}
                  className="img-responsive imageIntro"
                  alt="Image1"
                />
              </div>
              <p
                style={{
                  fontSize: "1.5vw",
                  marginBottom: "unset",
                  fontWeight: "700",
                  color: "#3578E5",
                }}
              >
                Giới thiệu
              </p>
              <br />
              {showDataUser()}
            </div>
          </div>
          <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">
            <StatusContent
              avatar={state.dataUser ? state.dataUser.user_avatar : ""}
              name={state.dataUser ? state.dataUser.user_name : ""}
            />
            {/* {GetStatus()} */}
            {ShowStatus()}
          </div>
        </div>
      )}
      {state.showFriendlist && (
        <div className="container-fluid">
          <div className="row" style={{ marginTop: "10px" }}>
            <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
              <h3 style={{ padding: "3px 20px" }}>Bạn bè</h3>
            </div>
            <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
              <input
                type="text"
                id="friendSearch"
                onChange={(e) => filterFriend(e)}
                placeholder="Tìm kiếm"
                className="form-control"
              />
            </div>
          </div>

          <div>
            <br />
            {showFriendlist()}
          </div>
        </div>
      )}
    </div>
  );
}

export default UnderHeaderProfile;
