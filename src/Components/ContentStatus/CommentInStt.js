import React, { Component } from "react";
import "./ContentStatus.css";
import NgoLanHuong1 from "../HinhAnh/NgoLanHuong1.jpg";
import ReactEmoji from "react-emoji";

class CommentInStt extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }
  render() {
    return (
      <div style={{ marginTop: "5px" }}>
        {/*  Khung chứa thông tin người đăng caption */}
        <img
          src={NgoLanHuong1} //props.Avatar
          width="40px"
          height="40px"
          className="ImageProfile"
          alt="facebook Profile Image1"
          style={{ float: "left", margin: "5px", marginleft: "0px" }}
        />
        <div className="InfoComment txtCmt">
          <span>
            <a href="# " style={{ fontWeight: "600" }}>
              {this.props.UserName} <br />
            </a>
            &nbsp;
            <span>
              <span>
                <span style={{ fontSize: "16px", wordBreak: "break-word" }}>
                  {ReactEmoji.emojify(this.props.comment)}
                </span>
              </span>
            </span>
          </span>
        </div>
      </div>
    );
  }
}
export default CommentInStt;
