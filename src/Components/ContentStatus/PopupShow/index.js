import React, { useEffect, useState } from "react";
import LCSModal from "./LCSModal";
import ContentModal from "./ContentModal";
const PopupShow = (props) => {
  // const [paging, props.setPaging] = useState(props.paging);
  const [OnPlay, setOnPlay] = useState(false);
  // useEffect(() => {
  //   props.setPaging(props.paging);
  // }, [props.paging]);
  if (document.getElementById(props.statusId + "-Modal")) {
    if (
      document.getElementById(props.statusId + "-Modal").className ===
      "modal fade"
    ) {
      document.querySelectorAll("video").forEach((vid) => vid.pause());
    }
  }
  const keydownModal = (event) => {
    if (event.keyCode === 27) {
      //ESC all video paused
      document.querySelectorAll("video").forEach((vid) => vid.pause());
    }

    if (event.keyCode === 37) {
      btnPrevious();
    }
    if (event.keyCode === 39) {
      console.log(props.paging);
      btnNext();
      console.log(props.fileSrc[props.paging], props.paging);
    }
  };
  const modal = document.getElementById(props.statusId + "-Modal");
  const btnPrevious = () => {
    if (props.paging <= 0) props.setPaging(props.fileSrc.length - 1);
    else props.setPaging((x) => x - 1);
    setOnPlay(false);
  };
  const btnNext = () => {
    if (props.paging >= props.fileSrc.length - 1) props.setPaging(0);
    else props.setPaging((x) => x + 1);
    setOnPlay(false);
  };
  useEffect(() => {
    if (modal) modal.addEventListener("keydown", keydownModal);
    return () => {
      if (modal) modal.removeEventListener("keydown", keydownModal);
    };
  }, [modal]); // eslint-disable-line react-hooks/exhaustive-deps
  const render1File = (file, i) => {
    // console.log(file);
    if (file.type === "image")
      return (
        <img
          src={file.uri}
          style={{
            margin: "auto",
            height: "100%",
          }}
          key={i}
          className="img-responsive"
          alt="statusFile"
        />
      );
    if (file.type === "video") {
      return (
        <div
          key={i}
          style={{
            display: "flex",
            flex: 1,
            position: "relative",
            backgroundColor: "black",
          }}
        >
          {!OnPlay && (
            <i
              id={"play-modal" + props.statusId + i.toString()}
              className="iconPlay"
              onClick={(e) => {
                document
                  .getElementById("video-modal" + props.statusId + i.toString())
                  .play();
                e.target.style.display = "none";
                if (
                  !document
                    .getElementById(
                      "video-modal" + props.statusId + i.toString()
                    )
                    .attributes.hasOwnProperty("controls")
                )
                  document
                    .getElementById(
                      "video-modal" + props.statusId + i.toString()
                    )
                    .toggleAttribute("controls");
              }}
            ></i>
          )}
          <video
            // autoPlay
            // controls
            onClick={(e) => {
              setOnPlay(true);
              if (e.target.paused) {
                e.target.play();
              }
              if (!e.target.attributes.hasOwnProperty("controls"))
                document
                  .getElementById("video-modal" + props.statusId + i.toString())
                  .toggleAttribute("controls");
            }}
            style={{ cursor: "pointer" }}
            onPlay={(e) => onPlayVideo(e)}
            onPause={() => setOnPlay(false)}
            key={i}
            width="100%"
            height="100%"
            id={"video-modal" + props.statusId + i.toString()}
          >
            <source src={file.uri} type="video/mp4" />
            Your browser does not support the video tag.
          </video>
        </div>
      );
    }
  };
  const onPlayVideo = (e) => {
    setOnPlay(true);
    document.querySelectorAll("video").forEach((vid) => {
      if (vid === e.target) vid.play();
      else vid.pause();
    });
  };

  if (props.fileSrc.length === 0) {
    return <div></div>;
  }
  return [
    <button
      type="button"
      className="btn btn-primary"
      style={{ display: "none" }}
      id={props.statusId + "-btnShowModal"}
      data-toggle="modal"
      data-target={"#" + props.statusId + "-Modal"}
    >
      Launch demo modal
    </button>,
    <div
      className="modal fade"
      id={props.statusId + "-Modal"}
      name={"Status-Modal"}
      tabIndex="-1"
      role="dialog"
      aria-labelledby="exampleModalLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-fullscreen" role="document">
        <div className="modal-content modal-fullscreen">
          <div
            className="modal-body"
            style={{ position: "relative", height: "100%", padding: 0 }}
          >
            <div style={{ display: "flex", width: "100%", height: "100%" }}>
              <div className="showImgAndVideo">
                <button
                  onClick={() =>
                    document.querySelectorAll("video").forEach((vid) => {
                      vid.pause();
                    })
                  }
                  type="button"
                  data-dismiss="modal"
                  aria-label="Close"
                  className="close btn-close-modal"
                >
                  <i className="fas fa-times-circle"></i>
                </button>
                {props.fileSrc.length > 1 && (
                  <div>
                    <button
                      id="btnPrevMedia"
                      style={{ display: "block" }}
                      type="button"
                      onClick={() => btnPrevious()}
                      className="btn btn-secondary btnDirection btnLeft"
                    >
                      <i className="fas fa-caret-left"></i>
                    </button>
                    <button
                      style={{ display: "block" }}
                      type="button"
                      id="btnNextMedia"
                      onClick={() => btnNext()}
                      className="btn btn-primary btnDirection btnRight"
                    >
                      <i className="fas fa-caret-right"></i>
                    </button>
                  </div>
                )}
                <div
                  style={{
                    display: "flex",
                    width: "100%",
                    height: "100%",
                  }}
                >
                  {render1File(props.fileSrc[props.paging], props.paging)}
                </div>
              </div>
              <div id={props.statusId + "-righth-Modal"} className="LCS-modal">
                <ContentModal statusId={props.statusId} />
                <LCSModal
                  statusId={props.statusId}
                  textid={props.statusId + "-Modal"}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>,
  ];
};

export default PopupShow;
