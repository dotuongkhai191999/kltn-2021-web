import React from "react";
import { useSelector } from "react-redux";
import PublicImage from "../../HinhAnh/Public.png";
export default function ContentModal(props) {
  const allStatus = useSelector((state) => state.HomePage);
  const singleStatus = allStatus.srcData.find((x) => x.id === props.statusId);
  //   console.log(singleStatus);
  //   useEffect(() => {
  //     const setStatusfirst = (value) => {
  //       if (value === "pub") {
  //         setState({ ...state, src: PublicImage, alt: "PublicIcon" });
  //         return;
  //       }
  //       if (value === "priv") {
  //         setState({
  //           ...state,
  //           src: PrivateICon,
  //           alt: "PrivateIcon",
  //         });
  //         return;
  //       }
  //       if (value === "friend") {
  //         setState({
  //           ...state,
  //           src: FriendIcon,
  //           alt: "FriendIcon",
  //         });
  //         return;
  //       }
  //     };

  //     setStatusfirst(singleStatus.status_setting);
  //   }, []);
  return (
    <div id={props.statusId + "header-Modal"}>
      <div style={{ display: "flex" }}>
        <img
          src={singleStatus.user_avatar}
          width="40px"
          height="40px"
          className="ImageProfile"
          alt="facebook Profile Image1"
          style={{ margin: "5px", display: "flex", flex: 1 }}
        />
        <div
          style={{
            fontWeight: 600,
            fontSize: 16,
            display: "flex",
            flex: 11,
            height: "fit-content",
            marginLeft: "4px",
            flexDirection: "column",
          }}
        >
          <h2
            style={{
              fontWeight: 600,
              fontSize: 16,
              margin: 0,
              display: "flex",
              flex: 1,
              height: "fit-content",
              marginTop: "8px",
              alignItems: "baseline",
            }}
          >
            <a
              href={"/" + singleStatus.no_sign_profile}
              style={{
                color: "black",
                fontWeight: 600,
              }}
            >
              {singleStatus.user_name}
            </a>
            &nbsp;
            <span style={{ fontSize: "13px", fontWeight: "normal" }}>
              {singleStatus.header_content}
            </span>
          </h2>
          <div style={{ display: "flex", flex: 1, alignItems: "center" }}>
            <span
              style={{
                fontSize: "13px",
                fontWeight: "normal",
                width: "fit-content",
                alignItems: "center",
              }}
            >
              15 phút
            </span>

            <img
              src={PublicImage}
              className="img-responsive"
              alt="Image1"
              style={{ width: "12px", height: "12px", margin: "0 10px" }}
            />
          </div>
        </div>
      </div>
      <p
        style={{
          marginLeft: 5,
          fontSize: 14,
          color: "#050505",
        }}
      >
        {singleStatus.caption}
      </p>
    </div>
  );
}
