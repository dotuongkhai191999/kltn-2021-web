import React, { useEffect } from "react";
import CommentInStt from "../CommentInStt";
import fbCmtIcon from "../../HinhAnh/fbCmtIcon.png";
import fbLikeIcon from "../../HinhAnh/fbLikeIcon.png";
import fbLikeBlue from "../../HinhAnh/fbLikeBlue.png";
import TextareaAutosize from "react-textarea-autosize";
import { useDispatch, useSelector } from "react-redux";
import "../ContentStatus.css";
import { Like_Redux, Comment_Redux } from "../../Redux/Actions/Home.Action";
const LCSModal = (props) => {
  const allStatus = useSelector((state) => state.HomePage);
  const singleStatus = allStatus.srcData.find((x) => x.id === props.statusId);
  const dispatch = useDispatch();

  useEffect(() => {
    const overflowDiv = document.getElementById(
      props.statusId + "-righth-Modal"
    );
    overflowDiv.scrollTop = overflowDiv.scrollHeight; //, options: "auto" }); // = ;
  });
  const onKeyDown = (e) => {
    // const arr = [...singleStatus.userCmt];

    if (e.key === "Enter") {
      if (e.target.value.trim() !== "") {
        dispatch(
          Comment_Redux(
            props.statusId,
            {
              avatar: localStorage.getItem("avatar"),
              username: "Khải Khù Khờ",
              cmt_content: e.target.value,
            },
            allStatus.srcData
          )
        );
      }
      // singleStatus.userCmt = arr;
      document.getElementById(props.statusId + "textCmt-Modal").value = "";
      if (e.preventDefault) e.preventDefault();
    }
  };
  const commentElement = () => {
    if (singleStatus.userCmt) {
      return singleStatus.userCmt.map((userCmt, index) => {
        return (
          <div key={index}>
            <CommentInStt
              userAvatar={userCmt.avatar}
              UserName={userCmt.username}
              comment={userCmt.cmt_content}
            />
          </div>
        );
      });
    }
  };
  const Like = () => {
    dispatch(
      Like_Redux(
        props.statusId,
        singleStatus.who_liked_status.length,
        singleStatus.liked,
        allStatus.srcData
      )
    );
    // const route = "status/update-status";
    // const likeLength = likeNum;
    // var param = {
    //   status_id: props.statusId,
    //   like: likeLength + 1,
    // };
    // const header = {
    //   Authorization: "bearer" + localStorage.getItem("UserToken"),
    // };
    // var api = new API();
    // if (!checkLike) {
    //   // chua co like
    //   setSrc(fbLikeBlue);
    //   setCheckLike(true);
    //   setLikeNum(likeNum + 1);
    //   api
    //     .onCallAPI("post", route, {}, param, header)
    //     .then((res) => {
    //       if (res.data.error_code !== 0) {
    //         window.alert(res.data.message);
    //       } else {
    //         setArrLike(res.data.data);
    //       }
    //     })
    //     .catch((err) => {
    //       console.log(err);
    //     });
    // } else {
    //   // đã like
    //   setSrc(fbLikeIcon);
    //   setCheckLike(false);
    //   setLikeNum(likeNum - 1);
    //   param.like = likeLength - 1;
    //   param.is_unlike = 1;
    //   api
    //     .onCallAPI("post", route, {}, param, header)
    //     .then((res) => {
    //       if (res.data.error_code !== 0) {
    //         window.alert(res.data.message);
    //       } else {
    //         setArrLike(res.data.data);
    //       }
    //     })
    //     .catch((err) => {
    //       console.log(err);
    //     });
    // }
  };
  const showUserLike = () => {
    var userLike = "";
    var length = singleStatus.who_liked_status
      ? singleStatus.who_liked_status.length
      : 0;
    if (length === 0) {
      return;
    }
    if (!singleStatus.who_liked_status) return;
    for (var i = 0; i < length; i++) {
      userLike += singleStatus.who_liked_status[i].user_name + ", ";
    }
    // console.log(userLike);

    if (length > 2) {
      userLike =
        singleStatus.who_liked_status[0].user_name +
        ", " +
        singleStatus.who_liked_status[1].user_name +
        " và " +
        (length - 2) +
        " người khác";
    }
    const iconLike =
      "data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' viewBox='0 0 16 16'%3e%3cdefs%3e%3clinearGradient id='a' x1='50%25' x2='50%25' y1='0%25' y2='100%25'%3e%3cstop offset='0%25' stop-color='%2318AFFF'/%3e%3cstop offset='100%25' stop-color='%230062DF'/%3e%3c/linearGradient%3e%3cfilter id='c' width='118.8%25' height='118.8%25' x='-9.4%25' y='-9.4%25' filterUnits='objectBoundingBox'%3e%3cfeGaussianBlur in='SourceAlpha' result='shadowBlurInner1' stdDeviation='1'/%3e%3cfeOffset dy='-1' in='shadowBlurInner1' result='shadowOffsetInner1'/%3e%3cfeComposite in='shadowOffsetInner1' in2='SourceAlpha' k2='-1' k3='1' operator='arithmetic' result='shadowInnerInner1'/%3e%3cfeColorMatrix in='shadowInnerInner1' values='0 0 0 0 0 0 0 0 0 0.299356041 0 0 0 0 0.681187726 0 0 0 0.3495684 0'/%3e%3c/filter%3e%3cpath id='b' d='M8 0a8 8 0 00-8 8 8 8 0 1016 0 8 8 0 00-8-8z'/%3e%3c/defs%3e%3cg fill='none'%3e%3cuse fill='url(%23a)' xlink:href='%23b'/%3e%3cuse fill='black' filter='url(%23c)' xlink:href='%23b'/%3e%3cpath fill='white' d='M12.162 7.338c.176.123.338.245.338.674 0 .43-.229.604-.474.725a.73.73 0 01.089.546c-.077.344-.392.611-.672.69.121.194.159.385.015.62-.185.295-.346.407-1.058.407H7.5c-.988 0-1.5-.546-1.5-1V7.665c0-1.23 1.467-2.275 1.467-3.13L7.361 3.47c-.005-.065.008-.224.058-.27.08-.079.301-.2.635-.2.218 0 .363.041.534.123.581.277.732.978.732 1.542 0 .271-.414 1.083-.47 1.364 0 0 .867-.192 1.879-.199 1.061-.006 1.749.19 1.749.842 0 .261-.219.523-.316.666zM3.6 7h.8a.6.6 0 01.6.6v3.8a.6.6 0 01-.6.6h-.8a.6.6 0 01-.6-.6V7.6a.6.6 0 01.6-.6z'/%3e%3c/g%3e%3c/svg%3e";
    return (
      <div className="positionLike" id="positionLike">
        <img
          src={iconLike}
          style={{ display: "inline-block", width: "18px", height: "18px" }}
          className="img-responsive"
          alt="Image1"
        />
        <a
          role="button"
          id="userLikes"
          href={"#modalUserLike" + props.statusId}
          data-toggle="modal"
        >
          &nbsp;
          {length <= 2 &&
            length > 0 &&
            userLike.substring(0, userLike.lastIndexOf(","))}
          {length > 2 && userLike}
        </a>
        <div
          className="modal fade"
          id={"modalUserLike" + props.statusId}
          tabIndex="-1"
          role="dialog"
          aria-labelledby="modalUserLikeLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <img
                  src={iconLike}
                  style={{
                    display: "inline-block",
                    width: "18px",
                    height: "18px",
                  }}
                  className="img-responsive"
                  alt="Image1"
                />
                <span style={{ fontWeight: "900" }}>&nbsp;{length}</span>
                <button
                  type="button"
                  className="close closeBtn"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                {singleStatus.who_liked_status.map((user, index) => {
                  return (
                    <div key={index} style={{ marginBottom: "5px" }}>
                      <img
                        src={user.avatar}
                        className="img-responsive"
                        style={{
                          width: "38px",
                          height: "38px",
                          border: "1px solid",
                          margin: "5px",
                          borderRadius: "100%",
                          display: "inline-block",
                        }}
                        alt="userAvatar"
                      />
                      <p
                        style={{
                          display: "inline-block",
                          fontSize: "15px",
                          fontWeight: "700",
                        }}
                      >
                        {user.user_name}
                      </p>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };

  const getfocus = () => {
    document.getElementById(props.statusId + "textCmt-Modal").focus();
  };
  return (
    <div style={{ padding: "0 5px" }}>
      <hr />
      {showUserLike()}
      <div className="KhungChuaLCS">
        <div style={{ display: "flex" }}>
          <button
            style={{ display: "flex", flex: 1, justifyContent: "center" }}
            type="button"
            className="btn btnLike"
            onClick={() => Like()}
          >
            <h5 className={singleStatus.liked ? "h5-btnLiked" : "h5-btnLike"}>
              <img
                src={singleStatus.liked ? fbLikeBlue : fbLikeIcon}
                alt="Like"
                style={{ marginRight: "5px" }}
              />
              <span style={{ marginTop: "4px" }}>
                Thích
                {singleStatus.who_liked_status.length
                  ? " " + singleStatus.who_liked_status.length
                  : ""}
              </span>
            </h5>
          </button>
          <button
            style={{ display: "flex", flex: 1, justifyContent: "center" }}
            type="button"
            className="btn btnCmt"
            onClick={(e) => getfocus(e)}
          >
            {/* onClick={() => getfocus()}> */}
            <h5 className="h5-btnCmt">
              <img src={fbCmtIcon} alt="cmt" style={{ marginRight: "5px" }} />
              Bình luận
            </h5>
          </button>
        </div>
        <div
          style={{
            width: "100%",
            height: "100%",
          }}
        >
          {commentElement()}
          <div style={{ display: "flex", width: "100%", alignItems: "center" }}>
            {/*  Khung chứa thông tin người đăng caption */}
            <div className="imgOfInfo">
              <img
                src={localStorage.getItem("avatar")}
                width="40px"
                height="40px"
                className="ImageProfile "
                alt="facebook Profile Image1"
              />
            </div>
            <div className="NameAndStatus txtcmttus">
              <TextareaAutosize
                maxRows={10}
                id={props.statusId + "textCmt-Modal"}
                name="txtCMT"
                className="form-control txtCmt"
                placeholder="Viết bình luận"
                onKeyDown={(e) => onKeyDown(e)}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LCSModal;
