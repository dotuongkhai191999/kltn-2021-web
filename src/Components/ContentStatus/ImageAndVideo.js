import React, { useRef, useState } from "react";
import "./ContentStatus.css";
import PopupShow from "./PopupShow/";
import { useSelector } from "react-redux";
export default function ImageAndVideo(props) {
  const HomePage = useSelector((state) => state.HomePage);
  const checkVideo = useRef(true);
  const status = HomePage.srcData.find((x) => x.id === props.statusId);
  const fileSrc = status.file_uploaded;
  const size = useRef(fileSrc.length);
  const [paging, setPaging] = useState(0);

  const render1File = (file, i) => {
    if (file.type === "image")
      return (
        <div
          key={i}
          style={{
            cursor: "pointer",
            width: size.current === 1 ? "" : "50%",
            height: size.current === 1 ? "" : "450px",
          }}
          onClick={() => {
            setPaging(i);
            setTimeout(() => {
              document.getElementById(props.statusId + "-btnShowModal").click();
            }, 500);
          }}
        >
          <img
            src={file.uri}
            style={{
              margin: "auto",
              width: "100%",
              height: "100%",
            }}
            className="img-responsive"
            alt="statusFile"
          />
        </div>
      );
    if (file.type === "video") {
      return (
        <div
          key={i}
          style={{
            display: "flex",
            position: "relative",
            width: size.current === 1 ? "" : "50%",
            backgroundColor: "rgba(0,0,0,.6)",
          }}
        >
          <i
            id={"play" + props.statusId + i.toString()}
            className="iconPlay"
            onClick={(e) => {
              document
                .getElementById("video" + props.statusId + i.toString())
                .play();
              e.target.style.display = "none";
              document
                .getElementById("video" + props.statusId + i.toString())
                .toggleAttribute("controls");
            }}
          ></i>
          <video
            style={{ cursor: "pointer" }}
            onClick={() => {
              setPaging(i);
              document.getElementById(props.statusId + "-btnShowModal").click();
            }}
            onPlay={(e) => onPlayVideo(e)}
            onPause={(e) => {
              e.target.toggleAttribute("controls");
              document.getElementById(
                "play" + props.statusId + i.toString()
              ).style.display = "";
            }}
            key={i}
            width="100%"
            height="100%"
            id={"video" + props.statusId + i.toString()}
          >
            <source src={file.uri} type="video/mp4" />
            Your browser does not support the video tag.
          </video>
        </div>
      );
    }
  };
  const onPlayVideo = (e) => {
    document.querySelectorAll("video").forEach((vid) => {
      if (vid === e.target) vid.play();
      else vid.pause();
    });
  };
  const render2File = (file) => {
    return (
      <div
        style={{
          flexDirection: checkVideo.current ? "row" : "unset",
          display: checkVideo.current ? "" : "flex",
        }}
      >
        {file.map((file, i) => {
          if (file.type === "image") {
            checkVideo.current = false;
          }
          return file.type === "image" ? (
            <div
              key={i}
              onClick={() => {
                setPaging(i);
                document
                  .getElementById(props.statusId + "-btnShowModal")
                  .click();
              }}
              className="render2image"
            >
              <img
                style={{ width: "100%", height: "100%" }}
                src={file.uri}
                className="img-responsive"
                alt="statusFile"
              />
            </div>
          ) : (
            <div
              key={i}
              style={{
                display: "flex",
                flex: 1,
                position: "relative",
                backgroundColor: "rgba(0,0,0,.6)",
              }}
            >
              <i
                id={"play" + props.statusId + i.toString()}
                className="iconPlay"
                onClick={(e) => {
                  document
                    .getElementById("video" + props.statusId + i.toString())
                    .play();
                  e.target.style.display = "none";
                  document
                    .getElementById("video" + props.statusId + i.toString())
                    .toggleAttribute("controls");
                }}
              ></i>
              <video
                style={{ cursor: "pointer" }}
                onClick={() => {
                  setPaging(i);
                  document
                    .getElementById(props.statusId + "-btnShowModal")
                    .click();
                }}
                onPlay={(e) => onPlayVideo(e)}
                onPause={(e) => {
                  e.target.toggleAttribute("controls");
                  document.getElementById(
                    "play" + props.statusId + i.toString()
                  ).style.display = "";
                }}
                key={i}
                width="100%"
                height="100%"
                id={"video" + props.statusId + i.toString()}
              >
                <source src={file.uri} type="video/mp4" />
                Your browser does not support the video tag.
              </video>
            </div>
          );
        })}
      </div>
    );
  };
  const render3File = (file) => {
    var videoArr = [];
    var arrFile = [...file];
    arrFile.map((x, i) => (x.index = i));
    for (var i = 0; i < arrFile.length; i++) {
      if (arrFile[i].type === "video") {
        videoArr.push(arrFile[i]);
        arrFile.splice(i, 1);
        i--;
      }
    }
    if (videoArr.length === 3) {
      const videoPlayBot = [...videoArr];
      videoPlayBot.shift();
      return (
        <div>
          <div
            style={{
              display: "flex",
              flex: 1,
              position: "relative",
              backgroundColor: "rgba(0,0,0,.6)",
            }}
          >
            <i
              id={"play" + props.statusId + i.toString()}
              className="iconPlay"
              onClick={(e) => {
                document
                  .getElementById("video" + props.statusId + i.toString())
                  .play();
                e.target.style.display = "none";
                document
                  .getElementById("video" + props.statusId + i.toString())
                  .toggleAttribute("controls");
              }}
            ></i>
            <video
              style={{ cursor: "pointer" }}
              onClick={() => {
                setPaging(i);
                document
                  .getElementById(props.statusId + "-btnShowModal")
                  .click();
              }}
              onPlay={(e) => onPlayVideo(e)}
              onPause={(e) => {
                e.target.toggleAttribute("controls");
                document.getElementById(
                  "play" + props.statusId + "0"
                ).style.display = "";
              }}
              width="100%"
              height="100%"
              id={"video" + props.statusId + "0"}
            >
              <source src={videoArr[0].uri} type="video/mp4" />
              Your browser does not support the video tag.
            </video>
          </div>
          <div style={{ display: "flex", marginTop: "2px", flex: 1 }}>
            {videoPlayBot.map((video, i) => {
              return (
                <div
                  key={i + 1}
                  style={{
                    display: "flex",
                    flex: 1,
                    position: "relative",
                    backgroundColor: "rgba(0,0,0,.6)",
                  }}
                >
                  <i
                    id={"play" + props.statusId + (i + 1).toString()}
                    className="iconPlay"
                    onClick={(e) => {
                      document
                        .getElementById(
                          "video" + props.statusId + (i + 1).toString()
                        )
                        .play();
                      e.target.style.display = "none";
                      document
                        .getElementById(
                          "video" + props.statusId + (i + 1).toString()
                        )
                        .toggleAttribute("controls");
                    }}
                  ></i>
                  <video
                    style={{ cursor: "pointer" }}
                    onClick={() => {
                      setPaging(i);
                      document
                        .getElementById(props.statusId + "-btnShowModal")
                        .click();
                    }}
                    onPlay={(e) => onPlayVideo(e)}
                    onPause={(e) => {
                      e.target.toggleAttribute("controls");
                      document.getElementById(
                        "play" + props.statusId + (i + 1).toString()
                      ).style.display = "";
                    }}
                    key={i + 1}
                    width="100%"
                    height="100%"
                    id={"video" + props.statusId + (i + 1).toString()}
                  >
                    <source src={video.uri} type="video/mp4" />
                    Your browser does not support the video tag.
                  </video>
                </div>
              );
            })}
          </div>
        </div>
      );
    }
    return (
      <div
        style={{
          display: videoArr.length === 1 ? "" : "flex",
          flexDirection: "row-reverse",
        }}
      >
        <div style={{ flex: 1 }}>
          {videoArr.map((video, i) => {
            return (
              <div
                key={i}
                style={{
                  display: "flex",
                  flex: 1,
                  position: "relative",
                  backgroundColor: "rgba(0,0,0,.6)",
                }}
              >
                <i
                  id={"play" + props.statusId + i.toString()}
                  className="iconPlay"
                  onClick={(e) => {
                    document
                      .getElementById("video" + props.statusId + i.toString())
                      .play();
                    e.target.style.display = "none";
                    document
                      .getElementById("video" + props.statusId + i.toString())
                      .toggleAttribute("controls");
                  }}
                ></i>
                <video
                  style={{ cursor: "pointer" }}
                  onClick={() => {
                    setPaging(video.index);
                    setTimeout(() => {
                      document
                        .getElementById(props.statusId + "-btnShowModal")
                        .click();
                    }, 100);
                  }}
                  onPlay={(e) => onPlayVideo(e)}
                  onPause={(e) => {
                    e.target.toggleAttribute("controls");
                    document.getElementById(
                      "play" + props.statusId + i.toString()
                    ).style.display = "";
                  }}
                  key={i}
                  width="100%"
                  height="100%"
                  id={"video" + props.statusId + i.toString()}
                >
                  <source src={video.uri} type="video/mp4" />
                  Your browser does not support the video tag.
                </video>
              </div>
            );
          })}
        </div>

        <div
          style={{
            display: "flex",
            flex: arrFile.length === 3 ? "" : 1,
            justifyContent: "space-around",
            marginTop: "2px",
          }}
        >
          {arrFile.map((file, i) => {
            return (
              <div
                key={i}
                onClick={() => {
                  setPaging(file.index);
                  document
                    .getElementById(props.statusId + "-btnShowModal")
                    .click();
                }}
                className="render2image"
              >
                <img
                  style={{ width: "100%", height: "100%" }}
                  src={file.uri}
                  className="img-responsive"
                  alt="statusFile"
                />
              </div>
            );
          })}
        </div>
      </div>
    );
  };
  const render4File = (file) => {
    const src4media = [...file];
    src4media.length = 4;
    return (
      <div
        style={{
          display: "flex",
          flexWrap: "wrap",
          flex: 1,
          position: "relative",
        }}
      >
        {src4media.map(render1File)}
        <div
          style={{
            width: "50%",
            backgroundColor: "rgba(0,0,0,.8)",
            position: "absolute",
            zIndex: 999,
            right: 0,
            bottom: 0,
            height: "450px",
            justifyContent: "center",
            display: "flex",
            alignItems: "center",
            cursor: "pointer",
          }}
          onClick={() => {
            setPaging(3);
            setTimeout(() => {
              document.getElementById(props.statusId + "-btnShowModal").click();
            }, 500);
          }}
        >
          <p style={{ fontWeight: "bold", fontSize: "50px", color: "white" }}>
            +{file.length - 4}
          </p>
        </div>
      </div>
    );
  };
  if (fileSrc.length !== 0)
    return [
      fileSrc.length === 1 && render1File(fileSrc[0], 0),
      fileSrc.length === 2 && render2File(fileSrc),
      fileSrc.length === 3 && render3File(fileSrc),
      fileSrc.length >= 4 && render4File(fileSrc),
      <PopupShow
        srcAVT={status.srcAVT}
        caption={status.caption}
        statusId={props.statusId}
        fileSrc={fileSrc}
        paging={paging}
        setPaging={setPaging}
        userCmt={status.userCmt}
        liked={status.liked}
        whoLike={status.who_liked_status}
      />,
    ];
  else return <div></div>;
}
