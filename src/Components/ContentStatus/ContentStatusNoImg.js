import React, { useEffect, useState } from "react";
import PublicImage from "../HinhAnh/Public.png";
import FriendIcon from "../HinhAnh/FriendIcon.png";
import PrivateICon from "../HinhAnh/PrivateICon.png";
import dropdownIcon from "../HinhAnh/sort_down.png";
import trashcan from "../HinhAnh/trash_can_25px.png";
import DivHaveLCS from "./DivHaveLCS.js";
import "./ContentStatus.css";
import API from "../API/API";
import ImageAndVideo from "./ImageAndVideo";
import { useSelector } from "react-redux";
const ContentStatusNoImg = (props) => {
  const homeState = useSelector((state) => state.HomePage);
  const [state, setState] = useState({
    src: PublicImage,
    alt: "PublicIcon",
  });

  const [loading, setLoading] = useState(false);
  const singleStatus = homeState.srcData.find((x) => x.id === props.statusId);
  useEffect(() => {
    const setStatusfirst = (value) => {
      if (value === "pub") {
        setState({ ...state, src: PublicImage, alt: "PublicIcon" });
        return;
      }
      if (value === "priv") {
        setState({
          ...state,
          src: PrivateICon,
          alt: "PrivateIcon",
        });
        return;
      }
      if (value === "friend") {
        setState({
          ...state,
          src: FriendIcon,
          alt: "FriendIcon",
        });
        return;
      }
    };

    setStatusfirst(singleStatus.status_setting);
    if (props.notOwn === "true") {
      document.getElementById("dropdownIcon" + props.indexTus).style.display =
        "none";
      document.getElementById("trashStt" + props.indexTus).style.display =
        "none";
    }
  }, [props.indexTus, props.notOwn]); // eslint-disable-line react-hooks/exhaustive-deps
  const clickSetting = () => {
    if (props.notOwn === "true") {
      document.getElementById("setting" + props.indexTus).className =
        "StatusofCaption";
    }
  };
  const updateSettingStatus = (settingStatus) => {
    var route = "status/update-status";
    var params = {
      status_id: props.statusId,
      status_setting: settingStatus,
    };
    var headers = {
      Authorization: "bearer" + localStorage.getItem("UserToken"),
    };
    var api = new API();
    api
      .onCallAPI("post", route, {}, params, headers)
      .then((res) => {
        if (res.data.error_code !== 0) {
          window.alert(res.data.message);
        } else {
          // console.log(res.data.data);
          setLoading(false);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const loadSetting = (a) => {
    setLoading(true);
    setTimeout(() => {
      // setLoading(false);
      updateSettingStatus(a);
    }, 2000);
  };
  const setStatePrivate = () => {
    loadSetting("priv");
    setState({
      ...state,
      src: PrivateICon,
      alt: "PrivateIcon",
      class: "PrivateImageIcon",
    });
  };
  const setStatePublic = () => {
    loadSetting("pub");

    setState({
      ...state,
      src: PublicImage,
      alt: "PublicIcon",
      class: "PublicImageIcon",
    });
  };
  const setStateFriend = () => {
    loadSetting("friend");

    setState({
      ...state,
      src: FriendIcon,
      alt: "FriendIcon",
      class: "FriendImageIcon",
    });
  };

  const deleteStatus = () => {
    var route = "status/delete";
    var param = {
      status_id: props.statusId,
    };
    var header = {
      Authorization: "bearer" + localStorage.getItem("UserToken"),
    };
    var api = new API();
    api
      .onCallAPI("post", route, {}, param, header)
      .then((res) => {
        if (res.data.error_code !== 0) {
          window.alert(res.data.message);
        } else {
          //LoaddedSetting();
          // console.log(res.data.data);
          window.location.reload(false);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const styleBtnStatus = {
    width: "26px",
    paddingBottom: "2px",
  };

  return (
    <div className="panel panel-default">
      <div className="panel-body bodyPanel">
        <div>
          <div className="container-fluid InfoCaption">
            {/*  Khung chứa thông tin người đăng caption */}
            <div
              className="col-sm-1 ImgHeadingSTT"
              onClick={() =>
                (window.location.href = "/" + singleStatus.no_sign_profile)
              }
            >
              <img
                src={singleStatus.user_avatar}
                width="40px"
                height="40px"
                className="ImageProfile"
                alt="facebook Profile Image1"
                style={{ marginTop: "unset" }}
              />
            </div>

            <div className="col-sm-10 NameAndStatus">
              <span> </span>
              <a href={"/" + singleStatus.no_sign_profile}>
                <strong> {singleStatus.user_name} </strong>
              </a>
              <span> {singleStatus.header_content}</span>
              <div>
                <span className="label lblTgianDang">15 phút</span>

                <div
                  className="dropdown StatusofCaption"
                  id={"setting" + props.indexTus}
                  onClick={() => clickSetting()}
                >
                  <a
                    href="# "
                    className="btn btn-secondary dropdown-toggle sttBtnCapImg"
                    id="TrangThaibtn"
                    data-toggle="dropdown"
                    // aria-haspopup="true"
                    aria-expanded="false"
                  >
                    <div style={styleBtnStatus}>
                      {!loading && (
                        <div id="statusSetting">
                          <img src={state.src} alt={state.alt} width="50%" />
                          <img
                            src={dropdownIcon}
                            alt="dropdownIcon"
                            width="50%"
                            id={"dropdownIcon" + props.indexTus}
                          />
                        </div>
                      )}
                      <i
                        style={{ display: loading ? "inline-block" : "none" }}
                        className="fa fa-circle-o-notch fa-spin"
                        id="loadingSetting"
                      ></i>
                    </div>
                  </a>
                  <div
                    className="dropdown-menu"
                    aria-labelledby="dropdownMenuButton"
                  >
                    <button
                      type="button"
                      className="btnPublic"
                      onClick={() => setStatePublic()}
                    >
                      <img
                        src={PublicImage}
                        alt="Public"
                        width="17%"
                        height="17%"
                      />
                      Công khai
                    </button>

                    <button
                      type="button"
                      className="btnFriend"
                      onClick={() => setStateFriend()}
                    >
                      <img
                        src={FriendIcon}
                        alt="Friend"
                        width="17%"
                        height="17%"
                      />
                      Bạn bè
                    </button>

                    <button
                      type="button"
                      className="btnPrivate"
                      onClick={() => setStatePrivate()}
                    >
                      <img
                        src={PrivateICon}
                        alt="Private"
                        width="17%"
                        height="17%"
                      />
                      Chỉ mình tôi
                    </button>
                  </div>
                </div>
              </div>
            </div>
            <div style={{ float: "right" }}>
              <button
                className="btn btn-large btn-block btn-default"
                id={"trashStt" + props.indexTus}
                style={{ padding: "unset" }}
                data-toggle="modal"
                data-target={"#confirmDelete" + props.indexTus}
              >
                <img
                  style={{ width: "100%", height: "100%" }}
                  src={trashcan}
                  className="img-responsive"
                  alt="Image1"
                />
              </button>
              <div
                className="modal fade"
                id={"confirmDelete" + props.indexTus}
                tabIndex="-1"
                role="dialog"
                aria-labelledby="confirmDeleteLabel"
                aria-hidden="true"
              >
                <div className="modal-dialog" role="document">
                  <div className="modal-content">
                    <div className="modal-header">
                      <h5 className="modal-title" id="confirmDelete">
                        Cảnh báo !!!
                      </h5>
                      <button
                        type="button"
                        className="close closeBtn"
                        data-dismiss="modal"
                        aria-label="Close"
                      >
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div className="modal-body">
                      Bạn có chắc xoá status này không?
                    </div>
                    <div
                      className="modal-footer"
                      style={{ borderTop: "unset" }}
                    >
                      <button
                        type="button"
                        onClick={() => deleteStatus()}
                        className="btn btn-primary"
                      >
                        Có
                      </button>
                      <button
                        type="button"
                        className="btn btn-secondary"
                        data-dismiss="modal"
                      >
                        Không
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div style={{ width: "100%" }}>
            <p>{singleStatus.caption}</p>
            {/* {CheckImgFromdb(props.srcCaption)} */}
            <div style={{ maxHeight: "900px" }}>
              <ImageAndVideo statusId={props.statusId} />
            </div>
          </div>
        </div>
        <hr />
        <DivHaveLCS
          index={props.indexTus}
          statusId={props.statusId}
          textid={props.textid}
        />
      </div>
    </div>
  );
};

export default ContentStatusNoImg;
// caption: "1 video "
// created_at: []
// file_uploaded: [{…}]
// id: "6066f1f496b20004a052f7da"
// like_number: 0
// liked: false
// no_sign_profile: "dotuong.khai.01.09"
// posted_time: "02/04/2021 05:29:08 PM"
// status_setting: "pub"
// userCmt: (3) [{…}, {…}, {…}]
// user_avatar: "http://api.facebook-kltn.alphawolf.io/image/default.jpg"
// user_id: "6053383ffcf9e96c2e2298c4"
// user_name: "Đỗ Tường Khải"
// who_liked_status: []
// useEffect(() => {
//   console.log(homeState.srcData.find((x) => x.id === props.statusId));
// }, [props.statusId]);
