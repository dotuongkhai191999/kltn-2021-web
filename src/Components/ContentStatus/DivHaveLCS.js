import React from "react";
import "./ContentStatus.css";
import fbLikeIcon from "../HinhAnh/fbLikeIcon.png";
import fbCmtIcon from "../HinhAnh/fbCmtIcon.png";
import fbShareIcon from "../HinhAnh/fbShareIcon.png";
import fbLikeBlue from "../HinhAnh/fbLikeBlue.png";
import CommentInStt from "./CommentInStt.js";
import { useDispatch, useSelector } from "react-redux";
import { Comment_Redux, Like_Redux } from "../Redux/Actions/Home.Action";
// import API from "../API/API.js";
const DivHaveCLS = (props) => {
  const dispatch = useDispatch();
  const allStatus = useSelector((state) => state.HomePage);
  const singleStatus = allStatus.srcData.find((x) => x.id === props.statusId);

  // useEffect(() => {
  //   console.log(allStatus);
  // }, []);
  const showUserLike = () => {
    var userLike = singleStatus.who_liked_status.user_name
      ? singleStatus.who_liked_status.user_name
      : "";
    var length = singleStatus.who_liked_status
      ? singleStatus.who_liked_status.length
      : 0;
    if (length === 0) {
      return;
    }

    for (var i = 0; i < length; i++) {
      userLike += singleStatus.who_liked_status[i].user_name + ", ";
    }
    if (length > 2) {
      userLike =
        singleStatus.who_liked_status[0].user_name +
        ", " +
        singleStatus.who_liked_status[1].user_name +
        " và " +
        (length - 2) +
        " người khác";
    }
    const iconLike =
      "data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' viewBox='0 0 16 16'%3e%3cdefs%3e%3clinearGradient id='a' x1='50%25' x2='50%25' y1='0%25' y2='100%25'%3e%3cstop offset='0%25' stop-color='%2318AFFF'/%3e%3cstop offset='100%25' stop-color='%230062DF'/%3e%3c/linearGradient%3e%3cfilter id='c' width='118.8%25' height='118.8%25' x='-9.4%25' y='-9.4%25' filterUnits='objectBoundingBox'%3e%3cfeGaussianBlur in='SourceAlpha' result='shadowBlurInner1' stdDeviation='1'/%3e%3cfeOffset dy='-1' in='shadowBlurInner1' result='shadowOffsetInner1'/%3e%3cfeComposite in='shadowOffsetInner1' in2='SourceAlpha' k2='-1' k3='1' operator='arithmetic' result='shadowInnerInner1'/%3e%3cfeColorMatrix in='shadowInnerInner1' values='0 0 0 0 0 0 0 0 0 0.299356041 0 0 0 0 0.681187726 0 0 0 0.3495684 0'/%3e%3c/filter%3e%3cpath id='b' d='M8 0a8 8 0 00-8 8 8 8 0 1016 0 8 8 0 00-8-8z'/%3e%3c/defs%3e%3cg fill='none'%3e%3cuse fill='url(%23a)' xlink:href='%23b'/%3e%3cuse fill='black' filter='url(%23c)' xlink:href='%23b'/%3e%3cpath fill='white' d='M12.162 7.338c.176.123.338.245.338.674 0 .43-.229.604-.474.725a.73.73 0 01.089.546c-.077.344-.392.611-.672.69.121.194.159.385.015.62-.185.295-.346.407-1.058.407H7.5c-.988 0-1.5-.546-1.5-1V7.665c0-1.23 1.467-2.275 1.467-3.13L7.361 3.47c-.005-.065.008-.224.058-.27.08-.079.301-.2.635-.2.218 0 .363.041.534.123.581.277.732.978.732 1.542 0 .271-.414 1.083-.47 1.364 0 0 .867-.192 1.879-.199 1.061-.006 1.749.19 1.749.842 0 .261-.219.523-.316.666zM3.6 7h.8a.6.6 0 01.6.6v3.8a.6.6 0 01-.6.6h-.8a.6.6 0 01-.6-.6V7.6a.6.6 0 01.6-.6z'/%3e%3c/g%3e%3c/svg%3e";
    return (
      <div className="positionLike" id="positionLike">
        <img
          src={iconLike}
          style={{ display: "inline-block", width: "18px", height: "18px" }}
          className="img-responsive"
          alt="Image1"
        />
        <a
          role="button"
          id="userLikes"
          href={"#modalUserLike" + props.index}
          data-toggle="modal"
        >
          {length <= 2 &&
            length > 0 &&
            userLike.substring(0, userLike.lastIndexOf(","))}

          {length > 2 && userLike}
        </a>
        <div
          className="modal fade"
          id={"modalUserLike" + props.index}
          tabIndex="-1"
          role="dialog"
          aria-labelledby="modalUserLikeLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <img
                  src={iconLike}
                  style={{
                    display: "inline-block",
                    width: "18px",
                    height: "18px",
                  }}
                  className="img-responsive"
                  alt="Image1"
                />
                <span style={{ fontWeight: "900" }}>&nbsp;{length}</span>
                <button
                  type="button"
                  className="close closeBtn"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                {singleStatus.who_liked_status.map((user, index) => {
                  return (
                    <div key={index} style={{ marginBottom: "5px" }}>
                      <img
                        src={user.avatar}
                        className="img-responsive"
                        style={{
                          width: "38px",
                          height: "38px",
                          border: "1px solid",
                          margin: "5px",
                          borderRadius: "100%",
                          display: "inline-block",
                        }}
                        alt="userAvatar"
                      />
                      <p
                        style={{
                          display: "inline-block",
                          fontSize: "15px",
                          fontWeight: "700",
                        }}
                      >
                        {user.user_name}
                      </p>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };

  const Like = () => {
    dispatch(
      Like_Redux(
        props.statusId,
        singleStatus.who_liked_status.length,
        singleStatus.liked,
        allStatus.srcData
      )
    );
  };

  const getfocus = () => {
    document.getElementById(props.textid).focus();
  };
  // const handleSubmit = (event) => {
  //   event.preventDefault();
  //   return <CommentInStt comment={state.txtCMT} />;
  // };

  const onKeyDown = (e) => {
    if (e.key === "Enter") {
      if (e.target.value.trim() !== "")
        dispatch(
          Comment_Redux(
            props.statusId,
            {
              avatar: localStorage.getItem("avatar"),
              username: "Khải Khù Khờ",
              cmt_content: e.target.value,
            },
            allStatus.srcData
          )
        );
      // singleStatus.userCmt = arr;
      e.target.value = "";
      if (e.preventDefault) e.preventDefault();
    }
  };
  const element = () => {
    if (singleStatus.userCmt)
      return singleStatus.userCmt.map((userCmt, index) => {
        return (
          <div key={index}>
            <CommentInStt
              UserName={userCmt.username}
              comment={userCmt.cmt_content}
              userAvatar={userCmt.avatar}
            />
          </div>
        );
      });
  };
  return (
    <div>
      {showUserLike()}
      <div className="KhungChuaLCS">
        <button type="button" className="btn btnLike" onClick={() => Like()}>
          <h5 className={singleStatus.liked ? "h5-btnLiked" : "h5-btnLike"}>
            <img
              src={singleStatus.liked ? fbLikeBlue : fbLikeIcon}
              alt="Like"
              style={{ marginRight: "5px" }}
              width="15%"
              height="15%"
            />
            <span style={{ marginTop: "4px" }}>
              Thích
              {singleStatus.who_liked_status.length
                ? " " + singleStatus.who_liked_status.length
                : ""}
            </span>
          </h5>
        </button>
        <button type="button" className="btn btnCmt" onClick={() => getfocus()}>
          <h5 className="h5-btnCmt">
            <img
              src={fbCmtIcon}
              alt="cmt"
              style={{ marginRight: "5px" }}
              width="15%"
              height="15%"
            />
            Bình luận
          </h5>
        </button>
        <button type="button" className="btn btnShare">
          <h5 className="h5-btnShare">
            <img
              src={fbShareIcon}
              alt="Share"
              style={{ marginRight: "5px" }}
              width="15%"
              height="15%"
            />
            Chia sẻ
          </h5>
        </button>
      </div>
      {element()}

      {/* {state.showHidenComment && (
        <div>
          <CommentInStt UserName={props.name} comment={state.txtCMT} />
        </div>
      )} */}
      <form onSubmit={(e) => e.preventDefault()}>
        <div style={{ display: "flex", width: "100%", alignItems: "center " }}>
          {/*  Khung chứa thông tin người đăng caption */}
          <div className="imgOfInfo">
            <img
              src={localStorage.getItem("avatar")}
              width="40px"
              height="40px"
              className="ImageProfile "
              alt="facebook Profile Image1"
            />
          </div>
          <div className="NameAndStatus txtcmttus">
            <input
              type="text"
              multiple={true}
              id={props.textid}
              name="txtCMT"
              className="form-control txtCmt"
              placeholder="Viết bình luận"
              onKeyDown={onKeyDown}
            />
          </div>
        </div>
      </form>
    </div>
  );
};
export default DivHaveCLS;
