import React, { Component } from "react";
import Messenger from "../HinhAnh/messenger.png";
import "./header.css";
import API from "../API/API";
class MessengerFacebook extends Component {
  constructor(props) {
    super(props);

    this.state = {
      friendAddArray: [],
    };
  }

  componentDidMount() {
    var route = "chat/list-messages";
    var param = {
      token: localStorage.getItem("UserToken"),
    };
    var header = {
      Authorization: "bearer" + localStorage.getItem("UserToken"),
    };
    var api = new API();
    api.onCallAPI("get", route, {}, param, header).then((res) => {
      if (res.data.error_code !== 0) {
        window.alert(res.data.message);
      } else {
        if (res.data.data) {
          this.setState({ friendAddArray: res.data.data });
          //  console.log(res.data.data);
        }
      }
    });
    //document.getElementById("btnMessenger").click();
    //   console.log(res.data.data);
  }

  // document.getElementById("btnMessenger").click();

  render() {
    var { friendAddArray } = this.state;
    let friendAddElement = friendAddArray.map((friendAddArray, index) => {
      return (
        <li key={index} onClick={() => {
          window.location.href = "/chat?room=" + friendAddArray.room;
          localStorage.setItem(
            "sender-messenger-name",
            friendAddArray.friend_chat
          );
          localStorage.setItem(
            "sender-messenger-avatar",
            friendAddArray.avatar
          );
          localStorage.setItem(
            "sender-messenger-link",
            friendAddArray.partner_no_sign_profile
          );
        }}
          style={{ width: "100%", position: "relative", height: "72px", borderBottom: "1px solid rgba(0,0,0,.1)" }}>
          <div
            className="avatarChatNorti"
            style={{ padding: "unset", width: "fit-content" }}
          >
            <img
              src={friendAddArray.avatar}
              className="img-responsive"
              alt="Image1"
              style={{
                float: "left",
                borderRadius: "50%",
                width: "56px",
                height: "56px",
              }}
            />
          </div>
          <div className="contentChatNorti">
            <h3
              className="message-notification"
              style={{
                fontSize: "15px",
                lineHeight: "20px",
              }}
            >
              {friendAddArray.friend_chat}
            </h3>
            <p
              className="message-notification"
              style={{
                lineHeight: "18px",
                overflow: "hidden",
              }}
            >
              <span>
                {friendAddArray.isCurrent
                  ? "Bạn: " + friendAddArray.messRecent
                  : friendAddArray.friend_chat +
                  ": " +
                  friendAddArray.messRecent}
              </span>
              <br /> {friendAddArray.time}
            </p>
          </div>
        </li>
      );
    });
    return (
      <li>
        <div className="dropdown">
          <button
            className="btn btn-primary dropdown-toggle friendHeader"
            type="button"
            data-toggle="dropdown"
            id="btnMessenger"
          >
            <img
              src={Messenger}
              width="25px"
              height="25px"
              className="ImageProfile"
              style={{ marginTop: "unset" }}
              alt="facebook Profile Image1"
            />
          </button>
          <div className="dropdown-menu  dropdown-menu-right ulDropdownFriend">
            <ul>
              <li
                style={{
                  borderBottom: "1px solid rgba(0,0,0,.1)",
                  width: "100%",
                }}
              >
                <h3
                  style={{
                    fontSize: "15px",
                  }}
                >
                  Gần đây
                </h3>
              </li>
              {friendAddElement}
              <li className="liAllRequest">
                <a href={"/chat"} className="AllFriendRequest">
                  Xem tất cả
                </a>
              </li>
            </ul>
          </div>
        </div>
      </li>
    );
  }
}

export default MessengerFacebook;
