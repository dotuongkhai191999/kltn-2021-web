import React, { useState } from "react";
import TextareaAutosize from "react-textarea-autosize";
import "../css/Input.css";
import sendIcon from "../../../HinhAnh/sendIcon.png";

const Input = ({ setMessage, sendMessage, message, botFunction, room }) => {
  const [input, setInput] = useState(message);
  return (
    <div className="form1">
      <TextareaAutosize
        className="input1"
        id="input-message"
        placeholder="Nhập tin nhắn..."
        value={input}
        onChange={({ target: { value } }) => {
          setMessage(value.trimEnd());
          setInput(value);
          botFunction();
        }}
        onKeyPress={(event) => {
          if (event.key === "Enter") {
            sendMessage(event);
            setInput("");
          }
        }}
        onKeyUp={(e) => {
          if (e.ctrlKey && e.key === "Enter") {
            const inp = input;
            setInput(inp + "\n");
            setMessage(inp + "\n");
          }
        }}
      />
      <button
        className="sendButton"
        style={{ backgroundColor: "transparent" }}
        onClick={(e) => {
          sendMessage(e);
          setInput("");
        }}
      >
        <img
          src={sendIcon}
          style={{ width: "36px", height: "36px" }}
          className="img-responsive"
          alt="Send"
        />
      </button>
    </div>
  );
};

export default Input;
