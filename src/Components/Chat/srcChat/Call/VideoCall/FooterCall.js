// import WebcamStreamCapture from "../WebCamera";

import React, { Component } from "react";

export default class FooterCall extends Component {
  constructor(props) {
    super(props);

    this.state = {
      myVideoWidth: 100,
    };
  }
  closeMyCamera = () => {
    if (this.state.myVideoWidth === 100) this.setHideMyCam();
    if (this.state.myVideoWidth === 0) this.setVisibleMyCam();
  };
  setVisibleMyCam = () => {
    var current_value = this.state.myVideoWidth;
    if (current_value >= 100) {
      return;
    } else {
      current_value++;
      this.setState({ myVideoWidth: current_value });
      setTimeout(this.setVisibleMyCam, 10);
    }
  };
  setHideMyCam = () => {
    var current_value = this.state.myVideoWidth;
    if (current_value <= 0) {
      return;
    } else {
      current_value--;
      this.setState({ myVideoWidth: current_value });
      setTimeout(this.setHideMyCam, 10);
    }
  };
  render() {
    return (
      <div className="footerCall">
        <div style={{ flex: 1, display: "flex" }}></div>

        <div style={{ flex: 1, display: "flex", justifyContent: "center" }}>
          <>
            <div style={{ display: "flex", flex: 1, justifyContent: "center" }}>
              <button
                onClick={(e) => this.props.endCall(e)}
                type="button"
                style={{ backgroundColor: "red" }}
                className="btn btn-default btnChatLeft"
              >
                <i className="fas fa-phone-alt" style={{ color: "white" }}></i>
              </button>
            </div>
          </>
        </div>

        <div
          style={{
            flex: 1,
            display: "flex",
            height: "200px",
            justifyContent: "flex-end",
            position: "relative",
          }}
        >
          <video
            ref={this.props.myvideoRef}
            className="myVideoCam"
            autoPlay
            muted={true}
            style={{
              width: this.state.myVideoWidth + "%",
              backgroundColor: "rgba(0,0,0,0.5)",
            }}
          />
          <button
            type="button"
            style={{ right: this.state.myVideoWidth + "%" }}
            onClick={() => this.closeMyCamera()}
            className="btn btn-default btnChatLeft btnSeeSelf"
          >
            {this.state.myVideoWidth !== 0 ? (
              <i className="fas fa-chevron-right"></i>
            ) : (
              <i className="fas fa-chevron-left"></i>
            )}
          </button>
        </div>
      </div>
    );
  }
}
