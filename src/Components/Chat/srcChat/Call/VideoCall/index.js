import React, { useEffect, useRef, useState } from "react";
import Peer from "peerjs";
import { SOCKET } from "../../../../Config";
import "./ChatContent.css";
import queryString from "query-string";
import { Button } from "@material-ui/core";

function App(props) {
  const socket = SOCKET;
  const [otherEnd, setOtherEnd] = useState(false); // eslint-disable-line
  const [room, setRoom] = useState("");
  const { location } = window;
  const [userId, setUserId] = useState(null); // eslint-disable-line
  const [action, setAction] = useState(null);
  const [isVisibleWaitingModal, setIsVisibleWaitingModal] = useState(true); // eslint-disable-line

  const userName = localStorage.getItem("linkProfile");

  const videoGrid1 = document.getElementById("video-grid-1");
  const videoGrid2 = document.getElementById("video-grid-2");

  const answeRef = useRef(null);

  const myVideo = document.createElement("video");
  myVideo.muted = true;
  const peer = new Peer(undefined);
  useEffect(() => {
    socket.on("redirect-chat-page", () => {
      window.location.href = "/chat";
      console.log(2);
    });
  }, []);
  useEffect(() => {
    const { room } = queryString.parse(location.search);
    const { action } = queryString.parse(location.search);

    setAction(action);
    setRoom(room);

    peer.on("open", (id) => {
      if (room && action) {
        socket.emit("join-room", room, id, userName);
        setUserId(id);
      }
    });
  }, [location.search, action]);

  useEffect(() => {
    navigator.getUserMedia = (navigator.getUserMedia ||
      navigator.webkitGetUserMedia ||
      navigator.mozGetUserMedia || 
      navigator.msGetUserMedia);

    var getUserMedia = navigator.getUserMedia;
    getUserMedia(
      {
        video: true,
        // audio: true,
      },
      function (stream) {
        if (action === "call") {
          console.log(1);
          socket.emit("call-user", {
            uri: `/chat/video-call?room=${room}&action=answer`,
            name: localStorage.getItem("UserName"),
          });
          // t = setTimeout(() => {
          //   socket.emit("user-close-video-chat");
          //   window.location.href = "/chat";
          // }, 30000);
        }
    
        const video = document.createElement("video");
        
        //cam của mình
        addVideoStream(video, stream);
        // answer();
        // socket.on("user-connected", (userId) => {
        //   call(userId);
        // });
      },
      function (err) {
        console.log("Failed to get local stream", err);
      }
    );
    var t;
    
    return () => {
      // clearTimeout(t);
    };
  }, [location.search, action]);

  useEffect(() => {
    socket.on("user-connected", (userId) => {
      if(action === "call"){
        call(userId);
      }
    });
    if(action && action === "answer"){
      answer();
    }
  },[action])

  function call(userId) {
    navigator.getUserMedia = (navigator.getUserMedia ||
      navigator.webkitGetUserMedia ||
      navigator.mozGetUserMedia || 
      navigator.msGetUserMedia);

    var getUserMedia = navigator.getUserMedia;
    getUserMedia(
      {
        video: true,
        // audio: true,
      },
      function (stream) {
        if (userId) {
          console.log('here call');
          var call = peer.call(userId, stream);
          const video = document.createElement("video");
          call.on("stream", function (remoteStream) {
            //cam của mình
            addVideoStream(video, remoteStream);
          });
        }
      },
      function (err) {
        console.log("Failed to get local stream", err);
      }
    );
  }

  function answer() {
    navigator.getUserMedia = (navigator.getUserMedia ||
      navigator.webkitGetUserMedia ||
      navigator.mozGetUserMedia || 
      navigator.msGetUserMedia);
    var getUserMedia = navigator.getUserMedia;
    console.log('media: ', navigator)
    peer.on("call", function (call) {
      getUserMedia(
        {
          video: true,
          // audio: true,
        },
        function (stream) {
          const video = document.createElement("video");
          call.on("stream", function (remoteStream) {
            //cam của ng ta
            addVideoStream(video, remoteStream);
          });
          call.answer(stream);
        },
        function (err) {
          console.log("Failed to get local stream", err);
        }
      );
    });
  }

  function addVideoStream(video, stream, type) {
    if (video && stream && (videoGrid1||videoGrid2)) {
      video.srcObject = stream;
      video.addEventListener("loadedmetadata", () => {
        video.play();
      });
      if(action === 'answer'){
        console.log('here answer')
        videoGrid1.append(video);
      }
      if(action === 'call'){
        console.log('here call')
        videoGrid2.append(video);
      }

      // if(type === "me" || type === "call"){
      //   videoGrid1.append(video);
      // }else if(type === "answer"){
      //   answeRef.current.srcObject = stream
      // }
    }
  }

  return (
    <div className="outerContainerChat">
      <div id="video-grid-1"></div>
      <div id="video-grid-2"></div>
      <div style={{ marginBottom: 10 }}></div>
      <Button
        style={{
          borderRadius: 25,
          padding: 10,
          backgroundColor: "red",
          width: 100,
          position: "absolute",
          bottom: 20,
          left: "50%",
        }}
        onClick={() => {
          socket.emit("user-close-video-chat");
          window.location.href = "/chat";
        }}
      >
        <i
          className="fas fa-phone-alt"
          style={{
            color: "white",
          }}
        ></i>
      </Button>
    </div>
  );
}

export default App;
