import React from "react";
const BodyCall = (props) => {
  if (props.isPermission === 0) {
    return (
      <div className="askPermission deniedPermission">
        <i className="fas fa-times-circle iconDenied"></i>
        <p
          className="iconDenied"
          style={{ fontSize: "50px", wordBreak: "break-word" }}
        >
          You must allow us use your MICRO and CAMERA for this function
        </p>
      </div>
    );
  }

  return (
    <>
      {props.isPermission === 3 && <div className="askPermission"></div>}
      <div className="bodyCall">
        {props.isReply && !props.callEnded ? (
          <video
            playsInline
            ref={props.uservideoRef}
            muted={false}
            autoPlay
            style={{
              width: "100%",
              backgroundColor: "red",
              position: "absolute",
              zIndex: -1,
              // display: props.videoOn ? "" : "none",
            }}
          />
        ) : null}
      </div>
    </>
  );
};

export default BodyCall;
