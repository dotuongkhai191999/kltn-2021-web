import React, { useRef } from "react";
import Webcam from "react-webcam";
const WebcamStreamCapture = (props) => {
  const webcamRef = useRef(props.ref);

  return (
    <div className="blockVideoSelf">
      <Webcam audio={false} ref={webcamRef} />
    </div>
  );
};

export default WebcamStreamCapture;
