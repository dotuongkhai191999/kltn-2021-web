import React, { useState, useEffect } from "react";
import queryString from "query-string";
// import io from "socket.io-client";
import API from "../../../API/API";
import Messages from "../Messages/Messages";
import Input from "../Input/Input";
import InfoBar from "../InfoBar/InfoBar.js";
import ListChat from "../../../Chat/srcChat/listChat/ListChat.js";
import "../css/Chat.css";
import { SOCKET } from "../../../Config";

//const ENDPOINT = 'http://20.197.88.62:5000';
//const ENDPOINT = 'http://10.12.1.177:5000';
// const ENDPOINT = "http://192.168.1.107:5000";
///const ENDPOINT = 'http://20.197.88.62:5000';

//console.log(2);
const Chat = ({ location }) => {
  const [name, setName] = useState("");
  const [room, setRoom] = useState("");
  const [message, setMessage] = useState("");
  const [messages, setMessages] = useState([]);
  //const [firstMess, setfirstMess] = useState()

  // const socket = io("http://192.168.1.105:5000");
  // socket = io(ENDPOINT);
  let socket = SOCKET;

  useEffect(() => {
    const { room } = queryString.parse(location.search);
    const name = localStorage.getItem("linkProfile");

    setRoom(room);
    setName(name);
    //
    if (room) {
      socket.emit("join", { name, room }, (err) => {
        console.log(room, name);
        if (err) alert(err);
      });
    }
}, [location.search]); // eslint-disable-line react-hooks/exhaustive-deps
  useEffect(() => {
    var route = "chat/list-messages-in-group";
    var param;
    if (room) {
      param = {
        chat_group_id: room,
      };
      var api = new API();
      api.onCallAPI("get", route, {}, param, {}).then((res) => {
        if (res.data.error_code !== 0) {
          window.alert(res.data.message);
        } else {
          if (res.data.data) {
            // console.log(res.data.data);
            res.data.data.map((mess) => {
              var messageText = {
                user: mess.owner,
                text: mess.contents,
                avatar: mess.user_avatar,
              };
              setMessages((messages) => [...messages, messageText]);
              //setName(mess.owner)
              return 0;
            });
            //botFunction();
          }
        }
      });
    }
  }, [room]); // eslint-disable-line react-hooks/exhaustive-deps
  useEffect(() => {
    socket.on("message", (message) => {
      var messageText = {
        user: message.user,
        text: message.text,
        avatar: message.avatar,
      };
      console.log(message);
      setMessages((messages) => [...messages, messageText]);
    });
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const sendMessage = (event) => {
    event.preventDefault();
    if (message.trim() === "") return;
    const avatar = localStorage.getItem("avatar");

    if (message) {
      socket.emit("sendMessage", message, name, avatar, room, () => setMessage(""));
    }
    if (document.getElementById("mess-group")) {
      document.getElementById("mess-group").scrollTop =
        document.getElementById("mess-group").scrollHeight;
    }
    if (message) {
      var route = "chat/save-message";
      if (message.trim() === "") {
        return;
      }
      var param = {
        chat_group_id: room,
        content: message,
      };
      var header = {
        Authorization: "bearer" + localStorage.getItem("UserToken"),
      };
      var api = new API();

      api.onCallAPI("post", route, {}, param, header).then((res) => {
        if (res.data.error_code !== 0) {
          window.alert(res.data.message);
        } else {
          if (res.data.data) {
            // console.log(res.data.data);
          }
        }
      });
    }
  };
  function botFunction() {
    const objDiv = document.getElementById("mess-group");
    if (objDiv) {
      // setVisible(false);
      objDiv.scrollTo({
        behavior: "smooth",
        top: objDiv.scrollHeight,
      });
    }
  }

  return (
    <div className="outerContainer">
      <div className="container2">
        <ListChat />
      </div>
      <div className="container1">
        <InfoBar room={room} />
        <Messages
          style={{}}
          messages={messages}
          name={name}
          botFunction={botFunction}
        />

        <Input
          message={message}
          room={room}
          setMessage={setMessage}
          botFunction={botFunction}
          sendMessage={sendMessage}
        />
      </div>
    </div>
  );
};

export default Chat;
