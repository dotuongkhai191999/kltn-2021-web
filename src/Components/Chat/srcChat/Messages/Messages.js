import React, { useEffect, useRef, useState } from "react";
import Message from "./Message/Message";
import "../css/Messages.css";

function Messages({ messages, name, botFunction, style }) {
  const [visible, setVisible] = useState(false);
  const messengers = useRef();
  useEffect(() => {
    messengers.current.scrollTop = messengers.current.scrollHeight;
  }, [messages]);
  const checkScroll = () => {
    const objDiv = document.getElementById("mess-group");
    if (objDiv) {
      if (objDiv.scrollTop <= objDiv.scrollHeight - objDiv.clientHeight - 1)
        setVisible(true);
      else setVisible(false);
    }
  };
  // console.log(messages)
  return [
    <div
      ref={messengers}
      className="messages"
      onClick={() => {
        if (document.getElementById("input-message")) {
          document.getElementById("input-message").focus();
        }
      }}
      onScroll={() => checkScroll()}
      id="mess-group"
    >
      {messages.map((message, i) => (
        <div key={i}>
          <Message message={message} name={name} />
        </div>
      ))}
    </div>,
    visible && (
      <button
        // style={{ display: visible ? "block" : "none" }}
        className="btn btn-default"
        id="btnScrollDown"
        onClick={() => botFunction()}
      >
        <i
          className="fas fa-long-arrow-alt-down"
          style={{
            fontSize: "15px",
          }}
        ></i>
      </button>
    ),
  ];
}

export default Messages;
