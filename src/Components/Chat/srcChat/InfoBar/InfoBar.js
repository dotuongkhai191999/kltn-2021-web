import React from "react";
import { Link } from "react-router-dom";
import "../css/InfoBar.css";
var avatarFriend = localStorage.getItem("sender-messenger-avatar");
const InfoBar = ({ room }) => {
  // const callVideo = (e) => {
  //   SOCKET.emit("click-call", {
  //     avatar: localStorage.getItem("avatar"),
  //     from: localStorage.getItem("UserName"),
  //     room: room,
  //   });
  // };
  return (
    <div className="infoBar">
      <div className="leftInnerContainer">
        <img
          src={avatarFriend}
          className="img-responsive avatar-message"
          alt="Avatar"
        />

        <a
          type="button"
          className="btn btn-default btnCall"
          onClick={(e) => {
            if (!room) {
              e.preventDefault();
            }
          }}
          href={`/chat/video-call?room=${room}&action=call`}
        >
          <i className="fas fa-phone-alt"></i>
        </a>

        <Link to={"/" + localStorage.getItem("sender-messenger-link")}>
          <span className="spanInfoBar">
            {localStorage.getItem("sender-messenger-name")}
          </span>
        </Link>
      </div>
    </div>
  );
};

export default InfoBar;
