import React, { useEffect } from "react";
import "./newsfeed.css"; //import toan bo class bên app css qua
import ContentStatusNoImg from "../ContentStatus/ContentStatusNoImg";
import DashBar from "./DashBar";
import { useDispatch, useSelector } from "react-redux";
import StatusContent from "../UpStatus/StatusContent.js";
import LazyLoad from "react-lazyload";
import API from "../API/API";
import { ReloadHome } from "../Redux/Actions/Home.Action";
const Newsfeed = () => {
  const storeState = useSelector((state) => state.HomePage);
  const dispatch = useDispatch();

  useEffect(() => {
    // getStatusNF();
    const fillIntro = () => {
      const params = {
        type_search: "1",
        token: localStorage.getItem("UserToken"),
      };
      const route = "user/search-v1";
      const headers = {
        Authorization: "bearer" + localStorage.getItem("UserToken"),
      };
      var api = new API();
      api
        .onCallAPI("get", route, {}, params, headers)
        .then((res) => {
          if (res.data.error_code !== 0) {
            window.alert(res.data.message);
          } else {
            console.log(res.data.data);
            if (localStorage.getItem("avatar") !== res.data.data.avatar) {
              localStorage.setItem("avatar", res.data.data.user_avatar);
            }
            if (localStorage.getItem("Cover") !== res.data.data.user_cover) {
              localStorage.setItem("Cover", res.data.data.user_cover);
            }
            if (localStorage.getItem("UserName") !== res.data.data.user_name) {
              localStorage.setItem("UserName", res.data.data.user_name);
            }
          }
        })
        .catch((err) => {
          console.log(err);
        });
    };
    fillIntro();
    dispatch(ReloadHome());
  }, [dispatch]);

  const onShowStatus = () => {
    const users = storeState.srcData ? storeState.srcData : null;
    // console.log(users);
    // var users = [
    //   {
    //     caption: "3 video test ",
    //     created_at: [],
    //     file_uploaded: [
    //       {
    //         type: "video",
    //         uri:
    //           "http://api.facebook-kltn.alphawolf.io/api/get-image?name=c4swdI8x9nLRlnROJfSkgJ1lhv0wws.mp4&option=status",
    //       },
    //       {
    //         type: "video",
    //         uri:
    //           "http://api.facebook-kltn.alphawolf.io/api/get-image?name=wpEK45NeEFWxRVxmt294kGEZzqekA4.mp4&option=status",
    //       },
    //       {
    //         type: "video",
    //         uri:
    //           "http://api.facebook-kltn.alphawolf.io/api/get-image?name=c4swdI8x9nLRlnROJfSkgJ1lhv0wws.mp4&option=status",
    //       },
    //     ],
    //     id: "605d6ed77de61a7588565c25",
    //     like_number: 0,
    //     liked: false,
    //     no_sign_profile: "dotuong.khai.01.09",
    //     posted_time: "26/03/2021 12:19:19 PM",
    //     status_setting: "pub",
    //     user_avatar:
    //       "http://api.facebook-kltn.alphawolf.io/api/get-image?name=default.jpg&option=avatars",
    //     user_id: "6053383ffcf9e96c2e2298c4",
    //     user_name: "Đỗ Tường Khải",
    //     who_liked_status: [],
    //   },
    // ];
    // return (
    //   // <></>
    //   <button
    //     type="button"
    //     className="btn btn-default"
    //     onClick={() =>
    //       dispatch(
    //         Like_Redux(
    //           "6066f1f496b20004a052f7da",
    //           "0",
    //           false,
    //           storeState.srcData
    //         )
    //       )
    //     }
    //   >
    //     button
    //   </button>
    // );
    if (users) {
      return users.map((user, index) => {
        return (
          <LazyLoad height={100} offset={100} key={index}>
            <ContentStatusNoImg
              notOwn="true"
              indexTus={index}
              statusId={user.id}
              textid={"textCmt" + user.id}
            />
          </LazyLoad>
        );
      });
    }
  };
  return (
    <div>
      <DashBar />
      <div className="container DashBarNAV" align="left"></div>
      <div className="container mainWall">
        <div className="createSTT">
          <StatusContent marginLeft="22%" />
        </div>
        <br />
        {onShowStatus()}
      </div>
    </div>
  );
};

export default Newsfeed;
