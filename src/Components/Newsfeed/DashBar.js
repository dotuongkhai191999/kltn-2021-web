import React, { Component } from 'react';
import './newsfeed.css'
class DashBar extends Component {
  render() {
    return (
      <div className="sidebar-menu">
        <a href={"/" + localStorage.getItem("linkProfile")} className="sidebar-item">
          <img src={localStorage.getItem("avatar")}
            width="28px" height="28px" style={{ borderRadius: "100%" }}
            className="sidebar-icon"
            alt="avatar" />
          <span className="sidebar-text">{localStorage.getItem("UserName")}</span>
        </a>
        <a href={"/" + localStorage.getItem("linkProfile")} className="sidebar-item">
          <i className="fas fa-user-friends sidebar-icon"></i>
          <span className="sidebar-text">Bạn bè</span>
        </a>

        <a href="/chat" className="sidebar-item">
          <i className="fab fa-facebook-messenger sidebar-icon"></i>
          <span className="sidebar-text">Tin nhắn</span>
        </a>
        <a href="/updateinfo" className="sidebar-item">
          <i className="fas fa-wrench sidebar-icon"></i>
          <span className="sidebar-text">Cài đặt</span>
        </a>
        <a href="https://forms.gle/EuvfK9KHKD7a9duFA" target="_blank" rel="noopener noreferrer"
          className="sidebar-item">
          <i className="fas fa-tasks sidebar-icon"></i>
          <span className="sidebar-text">Ý kiến đóng góp</span>
        </a>

      </div>
    );
  }
}

export default DashBar;