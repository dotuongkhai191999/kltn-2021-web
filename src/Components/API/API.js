import axios from "axios";

class API {
  constructor() {
    // this.domain = 'http://api.facebook-kltn.alphawolf.io/api/'
    this.domain = "http://192.168.1.148:8000/api/";
    // this.domain ="http://10.11.98.119:8000/api/"
    //this.domain = "https://fb-rest-api.herokuapp.com/api"
    //ua3
  }
  onCallAPI = (method, url, data = {}, params = {}, headers = {}) => {
    return axios({
      method: method,
      url: this.domain + url,
      data: data,
      params: params,
      headers: headers,
    });
  };
}

export default API;
