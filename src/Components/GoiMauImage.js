import API from './API/API.js'
import React, { Component } from "react";
class GoiMauImage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: null,

        }

    }
    test() {
        var route = "upload";
        var params = {
            user_id: "5f5c5c17ba3700002b00501c",
            type: "files",
            option: "avatars",
        }
        var api = new API();
        var formData = new FormData();
        var imagefile = document.querySelector('#file');
        formData.append("file", imagefile.files[0]);

        var header = {
            Authorization: ""
        }
        api.onCallAPI('post', route, formData, params, header).then(res => {
            if (res.data.error_code !== 0) {
                console.log(res.data)
            } else {
                console.log(res.data.data)
            }
        }).catch(err => {
            console.log(err)
        })
    }
    handleChange = (e) => {
        let file = e.target.files[0];
        if (!file) {
            return
        }
        else {
            if (file.size < 25e6) {
                console.log("25MB")
            }
            if (file.size > 25e6) {
                window.alert("lon hon 25MB")
                return false;
            }
        }
    }
    render() {
        return (
            <div>
                <div className="form-group">
                    <input type="file" accept='image/*' className="form-control" onChange={(e) => this.handleChange(e)}
                        id="file" />
                </div>
                <button onClick={() => this.test()} className="btn btn-primary">Submit</button>
            </div>
        );
    }
}
export default GoiMauImage;