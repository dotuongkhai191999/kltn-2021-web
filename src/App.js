import React, { Component } from "react";
import "./App.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Newsfeed from "./Components/Newsfeed/Newsfeed.js";
import ProfilePage from "./Pages/ProfilePage.js";
import UpdateInFo from "./Pages/UpdateInFo.js";
import LoginPage from "./Pages/LoginPage.js";
import StrangeFriend from "./Components/Profile/StrangeFriend.js";
import API from "./Components/API/API.js";
import HeaderNF from "./Components/HeaderPage/HeaderNF";
import ChatComponent from "./Components/Chat/srcChat/Chat/Chat.js";
import FanPages from "./Pages/FanPages";
import { Provider as StoreProvider } from "react-redux";
import store from "./Components/Redux/Store";
import VideoCall from "./Components/Chat/srcChat/Call/";
import { SOCKET } from "../src/Components/Config";
class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      URLproFile: localStorage.getItem("linkProfile"),
      URLproFiletest: localStorage.getItem("linkProfile"),
      url: [],
      checkToken: false,
      someOneCall: false,
      showNotification: false,
      arrNotifitaoi: [],
      isAnswer: false,
      videoCallUri: null,
      caller: "",
    };
  }
  componentDidMount() {
    // console.log(this.state.URLproFile);
    SOCKET.emit("user-access-web");
    SOCKET.on("call-user", (data) => {
      document.getElementById("onlyBuckle").click();
      this.setState({
        videoCallUri: data.uri,
        caller: data.name,
      });
    });

    if (localStorage.getItem("UserToken")) {
      //document.cookie = "token=" + localStorage.getItem("UserToken")
      // console.log(document.cookie);
      var param = {
        api_token: localStorage.getItem("UserToken"),
      };
      var header = {
        Authorization: "bearer " + localStorage.getItem("UserToken"),
      };
      var route = "user/validate-log-in";
      var api = new API();
      api
        .onCallAPI("post", route, {}, param, header)
        .then((res) => {
          if (res.data.error_code !== 0) {
            localStorage.clear();
            window.location.href = "/login";
          } else {
            this.setState({ checkToken: true });
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }
    var route1 = "user/all";
    var api1 = new API();
    api1
      .onCallAPI("get", route1, {}, {}, {})
      .then((res) => {
        if (res.data.error_code !== 0) {
          window.alert(res.data.message);
        } else {
          this.setState({
            url: res.data.data,
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }
  componentDidUpdate = () => {
    if (window.location.href.includes("/login"))
      if (this.state.checkToken) {
        window.location.href = "/";
      }
    // if (!window.location.href.includes("/chat")) {
    //   localStorage.removeItem("sender-messenger-name");
    //   localStorage.removeItem("sender-messenger-link");
    //   localStorage.removeItem("sender-messenger-avatar");
    // }
  };
  test = () => {
    if (localStorage.getItem("linkProfile")) {
      var { url } = this.state;
      for (var i = 0; i < url.length; i++) {
        if (url[i].no_sign_profile === localStorage.getItem("linkProfile")) {
          break;
        }
      }
      this.state.url.splice(i, 1);
    }
    return (
      localStorage.getItem("UserToken") &&
      this.state.url.map((route, index) => {
        return (
          <Route path={"/" + route.no_sign_profile} key={index} exact>
            <StrangeFriend userId={route.user_id} />
          </Route>
        );
      })
    );
  };
  answerCall = () => {
    const { videoCallUri } = this.state;
    localStorage.setItem("isAnswer", true);
    if (videoCallUri) {
      window.location.href = videoCallUri;
    }
  };
  myFunction = () => {
    var x = document.getElementById("snackbar");
    x.className = "show";
    setTimeout(function () {
      x.className = x.className.replace("show", "");
    }, 3000);
  };

  render() {
    return (
      <StoreProvider store={store}>
        <div>
          {/* <ChatBox /> */}
          <Router>
            <button
              type="button"
              className="btn btn-primary"
              data-toggle="modal"
              style={{ display: "none" }}
              data-target="#staticBackdrop"
              id="onlyBuckle"
            >
              Còn mỗi cái nịt
            </button>
            <div
              className="modal fade"
              id="staticBackdrop"
              data-backdrop="static"
              tabIndex="-1"
              role="dialog"
              aria-labelledby="staticBackdropLabel"
              aria-hidden="true"
            >
              <div className="modal-dialog" role="document">
                <div className="modal-content">
                  <div className="modal-body">
                    <h3 style={{ textAlign: "center" }}>
                      {this.state.caller} đang gọi bạn!
                    </h3>
                  </div>
                  <div className="modal-footer" style={{ border: 0 }}>
                    <button
                      type="button"
                      className="btn btn-secondary"
                      data-dismiss="modal"
                      style={{ backgroundColor: "rgba(0,0,0,0.2)" }}
                      onClick={() => {
                        SOCKET.emit("user-close-video-chat");
                        window.location.reload(false);
                        // window.location.href = "/";
                      }}
                    >
                      Từ chối
                    </button>
                    <button
                      type="button"
                      onClick={() => this.answerCall()}
                      className="btn btn-primary"
                    >
                      Chấp nhận
                    </button>
                  </div>
                </div>
              </div>
            </div>
            <a href="/login" id="loginAtag" style={{ display: "none" }}>
              ờ may dinh, gút chóp em
            </a>
            {localStorage.getItem("UserToken") && this.state.checkToken && (
              <HeaderNF />
            )}
            {!localStorage.getItem("UserToken") && <LoginPage />}
            {localStorage.getItem("UserToken") && (
              <Switch>
                <Route path="/" exact component={Newsfeed} />

                <Route path={"/" + this.state.URLproFiletest} exact>
                  <ProfilePage friend={false} />
                </Route>

                <Route path={"/" + this.state.URLproFiletest + "/friend"} exact>
                  <ProfilePage friend={true} />
                </Route>

                {this.test()}

                <Route path="/chat" exact component={ChatComponent} />

                <Route path="/pages" component={FanPages} />

                <Route
                  path="/chat/video-call"
                  exact
                  render={() => <VideoCall alo={this.state.isAnswer} />}
                />

                <Route path="/UpdateInfo" component={UpdateInFo} />

                <Route
                  render={() => (
                    <h1 style={{ marginTop: "50px" }}>
                      Không có trang bạn cần tìm
                    </h1>
                  )}
                />
              </Switch>
            )}
          </Router>
        </div>
      </StoreProvider>
    );
  }
}

export default App;
