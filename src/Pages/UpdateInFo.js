import React, { Component } from 'react';
import './UpdateInfo.css';
import UpdateInfoComponent from '../Components/Setting/UpdateInfoComponent.js'
import API from '../Components/API/API';
class UpdateInFo extends Component {
    constructor(props) {
        super(props)

        this.state = {

        }
    }
    openCity = (evt, cityName) => {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent1");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }
    componentDidMount() {
        document.getElementById("defaultOpen").click();
    }
    handleSubmitPass = (e) => {
        e.preventDefault();
        var param = {
            update_type: 1,
            old_password: document.getElementById("oldPassword").value,
            new_password: document.getElementById("confirmPass").value,
            token: localStorage.getItem("UserToken")
        }
        var route = "user/update/info";
        var header = {
            Authorization: 'bearer' + localStorage.getItem("UserToken")
        };
        var api = new API();
        api.onCallAPI('post', route, {}, param, header).then(res => {
            if (res.data.error_code !== 0) {
                window.alert(res.data.message)
            } else {
                window.alert(res.data.data);
                document.getElementById("validPass").style.display = "none"
                document.getElementById("btnChangePass").disabled = true;
                document.getElementById("resetForm").click();
            }
        }).catch(err => {
            console.log(err)
        })
    }
    changePassword = () => {
        document.getElementById("validPass").style.display = "block"

        if (document.getElementById("newPassword").value !== document.getElementById("confirmPass").value) {
            document.getElementById("validPass").className = "glyphicon glyphicon-remove"
            document.getElementById("btnChangePass").disabled = true;
        }
        else {
            document.getElementById("validPass").className = "glyphicon glyphicon-ok"
            document.getElementById("btnChangePass").disabled = false;
        }
    }
    render() {
        return (<div id="updateInfo" style={{ marginTop: "50px" }}>
            <div className="container mainwall">
                <h3 className="h3Table"> Cài đặt tài khoản và thông tin cá nhân.</h3>
                <div className="tab1">
                    <button className="tablinks btnFixInfo"
                        onClick={(event) => this.openCity(event, 'changeInfo')}
                        id="defaultOpen">Chỉnh sửa thông tin cá nhân</button>
                    <button className="tablinks btnFixInfo"
                        onClick={(event) => this.openCity(event, 'changePassword')}
                    >Đổi mật khẩu</button>
                </div>
                <div id="loader"></div>

                <div id="changeInfo" className="tabcontent1">
                    <UpdateInfoComponent />
                </div>

                <div id="changePassword" className="tabcontent1">
                    <h3 style={{ padding: "10px", backgroundColor: "rgba(0,0,0,.05)" }} > Mật khẩu </h3>
                    <form onSubmit={this.handleSubmitPass}>
                        <table className="table table-hover" style={{ marginBottom: "unset" }}>
                            <tbody>
                                <tr>
                                    <td width="20%">
                                        Mật khẩu cũ:
                                </td>
                                    <td>
                                        <input type="password" id="oldPassword" className="form-control" required="required" title="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Mật khẩu mới:
                                </td>
                                    <td>
                                        <input type="password" id="newPassword"
                                            className="form-control" required="required" onChange={() => this.changePassword()} />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Xác nhận mật khẩu:
                                </td>
                                    <td>
                                        <input type="password" id="confirmPass" className="form-control" required="required"
                                            onChange={() => this.changePassword()} />
                                    </td>
                                    <td style={{ borderTop: "unset" }}>
                                        <span style={{ paddingTop: "10xpx", display: "none" }} id="validPass"></span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div style={{ float: "right", padding: "10px" }}>
                            <button type="submit" id="btnChangePass" disabled className="btn btn-primary">Lưu</button> &nbsp;
                        <button type="reset" id="resetForm" className="btn btn-default">Hủy</button>
                        </div>
                    </form>
                </div>
            </div>
        </div >
        );
    }
}

export default UpdateInFo;
